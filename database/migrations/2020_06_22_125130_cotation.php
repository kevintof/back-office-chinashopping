<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Cotation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('cotation', function(Blueprint $table)
        {
            $table->increments('id')->unsigned();
            $table->integer('montant');

            $table->integer('paiement_id')->unsigned();
            $table->foreign('paiement_id')
                ->references('id')
                ->on('transactions')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->text('date_retour_prevu')->nullable();
            $table->text('commentaire')->nullable();
            $table->string('fichier_in')->nullable();
            $table->string('fichier_out')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cotation');
    }
}
