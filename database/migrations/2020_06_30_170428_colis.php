<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Colis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('colis', function(Blueprint $table)
        {
            $table->increments('id')->unsigned();
            $table->string('reference', 100);
            $table->float('poids')->nullable();
            $table->integer('transport')->nullable();
            $table->integer('frais_livraison')->nullable();
        

            $table->integer('transaction_id')->nullable()->unsigned();
            $table->foreign('transaction_id')
                ->references('id')
                ->on('transactions')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->integer('type_livraison_id')->nullable()->unsigned();
            $table->foreign('type_livraison_id')
                ->references('id')
                ->on('automations')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->text('images')->nullable();
            $table->string('commentaires')->nullable();

            $table->integer('adresse_livraison_id')->unsigned()->default(1);
            $table->foreign('adresse_livraison_id')
                ->references('id')
                ->on('adresses')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->integer('details_commande_id')->unsigned();
            $table->foreign('details_commande_id')
                ->references('id')
                ->on('details_commandes')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->integer('lots_id')->nullable()->unsigned();
            $table->foreign('lots_id')
                ->references('id')
                ->on('lots')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('colis', function(Blueprint $table) {
            
            $table->dropForeign('colis_transaction_id_foreign');
            $table->dropForeign('colis_type_livraison_id_foreign');
            $table->dropForeign('colis_adresse_livraison_id_foreign');
            $table->dropForeign('colis_details_commandes_id_foreign');
            $table->dropForeign('colis_lots_foreign');
        });

        Schema::drop('colis');
    }
}
