<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Admin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            
            $table->increments('id')->unsigned();
            $table->string('nom');
            $table->string('prenoms');
            $table->string('email')->unique();
            $table->integer('pays_id')->unsigned();
            $table->foreign('pays_id')
                ->references('id')
                ->on('pays')
                ->onDelete('restrict')
                ->onUpdate('restrict');
                
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
