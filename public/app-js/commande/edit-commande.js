
 $(document).ready(function(){ 

  $('.edit-product').click(function(){
    console.log("modal");
    $('.modal-prod-id').attr('value',$(this).data('produitid')); 
     $('.modal-nom').attr('value',$(this).data('nom'));/* $(this).data() */
     $('.modal-nom').val($(this).data('nom'));
     $('.modal-description').val($(this).data('description'));
     $('.modal-quantite').val($(this).data('quantite'));
     $('.modal-lien').val($(this).data('lien'));
     console.log($(this).data('description'))
    })  
    $('.send-data').click(function(){
      change_product_primary_informations($('.modal-prod-id').val());
    })

    function change_product_primary_informations(id)
    {
      var nom = $('.modal-nom').val();
      console.log(nom);
      var description = $('.modal-description').val();
      var quantite = $('.modal-quantite').val();
      var lien = $('.modal-lien').val();
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
       });
       $.post({
        url:"/admin/commande/update_provided_informations/products/"+id,
        dataType: "json",
        data:{ pr_nom:nom,pr_description:description,pr_lien:lien,pr_quantite:quantite },
        success:function(data)
        {
         
          $('.description'+data.produit_id).html(data.description);
          $('.nom'+data.produit_id).html(data.nom);
          $('.quantite'+data.produit_id).html(data.quantite); 
          $( "button[data-produitid='"+data.produit_id+"']" ).attr("data-quantite",data.quantite);
          $( "button[data-produitid='"+data.produit_id+"']" ).attr("data-description",data.description);
          $( "button[data-produitid='"+data.produit_id+"']" ).attr("data-nom",data.nom);
          $( "button[data-produitid='"+data.produit_id+"']" ).attr("lien",data.lien);
          $('.produit'+data.produit_id).prepend('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h5><i class="icon fas fa-check"></i></h5>'+data.message+'</div>');
          $('#modal-default .close').click();
          $('.lien'+data.produit_id).attr('href',data.lien)
          
          console.log('cc');  
           
        },
        error: function(msg){
            
        }
       })

    }
 }) 