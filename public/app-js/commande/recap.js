function montantApayer(id) {

	toastbox('loadingToast');

	if ($("input[name='optionPaiement']:checked").val() == 'montant_total') {
		
		$.get({

	        url: '/checkout/getAmount-montant_total/'+id,
	        success: function (response) {

	        	$("#loadingToast").removeClass("show");
	            $("#montant").val(response.montant_total);
	            $("#transportInfo").html('');
	        },
	        error: function (response) {
	        }
	     });

	}else {
		
		$.get({

	        url: '/checkout/getAmount-montant_commande/'+id,
	        success: function (response) {
	        	$("#loadingToast").removeClass("show");
	            $("#montant").val(response.montant_commande);
	            $("#transportInfo").html(response.html);

	        },
	        error: function (response) {
	        }
	     });
	}
}


$( "#validateOrder" ).click(function() {
  	toastbox('security-message');
});