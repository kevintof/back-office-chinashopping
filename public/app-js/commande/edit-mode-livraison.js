$(document).ready(function(){
    $('.saveModeLivraison').click( function(){
        $('#edit-livraison').ajaxSubmit({
            resetForm: true,
            success: function(data){

              $('#modal-edit-livraison .close').click();
              $('#livraison').prepend('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h5><i class="icon fas fa-check"></i></h5>Le mode de livraison a été changé avec succès</div>') 
               $('.val_livraison').html(data.livraison);
            }
        })
    
     })
})