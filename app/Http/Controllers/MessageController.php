<?php

namespace App\Http\Controllers;
use App\Lib\PusherFactory;
use Illuminate\Http\Request;
use Pusher\Pusher;
use App\Message;
use App\Commandes;
use App\Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MessageController extends Controller
{
    public function sendMessage(Request $request,$id)
    {      
        $message = new Message();
        $notification = new Notification();
        
       
        $contenu = $request->input("contenu");
        
        $message->contenu = $contenu;
        $message->commande_id = $id;
        $message->date = date("Y-m-d H:i:s");;
        
        $data["contenu"] = $contenu;
        $data["date"] = date("d/m/Y H:i:s");
         $affected = null; 
        if(Auth::user() == null)
        {
          $message->is_admin = true;
          $data["is_admin"] = true;
          $data["sender"] = null;
          $data["sender_name"]= "admin";
          $data["commande_id"] = $id;
          $message->is_read_by_admin = true;
          //$notification->type = "NOUVEAU_MESSAGE";
          //$notification->etatLecture = false;
          //$notification->date =
              $notification = new Notification(); 
             //$user = DB::table('users')->where('name', 'John')->first();
       
             $notificationUpdate = DB::table('notifications')->where('commande_id',$id)->where('type','MESSAGERIE')->first(); 
            
            if($notificationUpdate!= null)
            {
               
              $affected = DB::table('notifications')
              ->where('id', $notificationUpdate->id)
              ->update(['date' => $message->date,'etatLecture'=>false]); 
              $data['notification_id'] = $notificationUpdate->id;
               
             
            }
            else{
              $notification->date = $message->date;
              $notification->etatLecture = false;
              $notification->type = "MESSAGERIE";
              $notification->commande_id = $id;
              $notification->save();
              $data['notification_id'] = $notification->id;
            } 
 

        }
        else{
            $message->is_admin = false;
            $data["is_admin"] = false;
            $data["sender"] = Auth::id();
            $data["sender_name"]= Auth::user()->nom;
            $data["commande_id"] = $id;
            $message->is_read_by_admin = false;
            $data["date"] = date("d/m/Y H:i:s");
		  }
		    //$commande = new Commmandes();
		    $data["user_id"] = $message->commande->user_id;
        $pusher = PusherFactory::make();
        $channel = 'tarzan-express';
        $event = 'chat';
        $data['event-source'] = $event;
        $data['reference'] = $message->commande->reference;
        
        $pusher->trigger($channel, $event, $data);
        $message->is_read = false;
        $message->save();
        return response()->json(['state' => 1, 'data' => $message]);

       
    }

    public function send_pictures_as_message($id, Request $request){
      
      $message = new Message();
       
        if ($request->file('cmd_pictures') != null) {
          $imageName = $request->file('cmd_pictures')->getClientOriginalName();
          $request->file('cmd_pictures')->move(public_path().'/messages-commandes/'.$id, $imageName);

          $message->image = $imageName;
      }    
      $message->commande_id = $id; 
      $message->type = "IMAGE";
      $message->date = date("Y-m-d H:i:s");
      if(Auth::user() == null)
      {
        $message->is_admin = true;
        $data["is_admin"] = true;
        $data["sender"] = null;
        $data["sender_name"]= "admin";
        $data["commande_id"] = $id;
        $data["image"] = request()->getHttpHost().'/messages-commandes/'.$id.'/'.$message->image;
        $message->is_read_by_admin = true;
        $data["date"] =  date("d/m/Y H:i:s");
      }
      else{
        $message->is_admin = false;
        $data["is_admin"] = false;
        $data["sender"] = Auth::id();
        $data["sender_name"]= Auth::user()->nom;
        $data["commande_id"] = $id;
        
        $data["date"] =  date("d/m/Y H:i:s");
        /* $data["image"] */
        $message->is_read_by_admin = false;
    
      }
      $message->is_read = false;
      $message->save();
      $data["message_id"] = $message->id;
      $data["user_id"] = $message->commande->user_id;
      $data["image"] = request()->getSchemeAndHttpHost().'/messages-commandes/'.$id.'/'.$message->image;
      
      $pusher = PusherFactory::make();
      $message->image = $data["image"];
      $data['reference'] = $message->commande->reference;   
      $event = 'chat';
      $data["event-source"] = $event; 
      $pusher->trigger('tarzan-express', $event, $data);
      return response()->json(['state' => 1, 'data' => $message]);
    }

    
    public function fetch_client_messages(Request $request, $id)
    {
      $commande = Commandes::find($id);
      $messages = Message::where('commande_id','=',$id)->get();
      $mess = DB::table('messages')->where('commande_id','$id')->get();

      $output = '<ul class="list-unstyled">';
    foreach($messages as $message)
    {
      $user_name = '';
      if($message->is_admin == true)
      {
      $user_name = '<b class="text-success"></b>';
      $output .= '
      <li class="round" style="border-bottom:1px dotted #ccc;margin-left:48px;margin-right:0">
      <div style="margin-right:0; padding:12px">'.$user_name.' &nbsp;&nbsp; ';
         if($message->type != 'IMAGE')
         {
           $output .= $message->contenu;
         }
         else{
           $output.= '<p><img src="/messages-commandes/'.$commande->id.'/'.$message->image.'" class="img-thumbnail" width="200" height="160" /></p><br />';
         }
      
      
      $output .='
        
      </div>
        
      </li>
      <div align="right">
         <small><em>'.$message->date.'</em></small>
        </div>
      <hr style="border:1px"/>
      ';
      }
      else
      {
       $user_name = '<b class="text-danger">'.$commande->user->nom.'</b>';
       $output .= '
       <li class="second" style="border-bottom:1px dotted #ccc;margin-right:48px">
       <div style="padding:12px">'.$user_name.' <br/> ';
         if($message->type != 'IMAGE') 
          {
            $output .= $message->contenu.'
         
            </div>
            
            </li>
            <div align="left">
               <small><em>'.$message->date.'</em></small>
              </div>
            <hr style="border:1px"/>
            '; 
          } 
          else{
            $output .= '
            <p><img src="/messages-commandes/'.$commande->id.'/'.$message->image.'" class="img-thumbnail" width="200" height="160" /></p><br />
         
            </div>
            
            </li>
            <div align="left">
               <small><em>'.$message->date.'</em></small>
              </div>
            <hr style="border:1px"/>
            ';
          }
       
      }
      
    }
    $output .= '</ul>';
    return response($output);

  }


        

}
