<?php

namespace App\Http\Controllers\User\Admin;
use App\Commandes;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Produits;
use App\DetailsCommande;
use App\Colis;

class ProduitsController extends Controller
{

	  
    public function update(Request $request, $id)
    {
    	
    	$produit = Produits::find($id);

    	if($produit) {

    		$produit->montant = $request->input('montant');
    		$produit->statut = $request->input('statut');
    		$produit->poids = $request->input('poids');
    		$produit->save();
		}


        return response()->json(['message' => 'Données enregistrées avec succès'], 200);
	}
	
	public function update_user_provided_informations(Request $request, $id)
	{
		$produit = Produits::find($id);

		if($produit)
		{
			$produit->quantite = $request->input('pr_quantite');
			$produit->description = $request->input('pr_description');
			$produit->lien = $request->input('pr_lien');
			$produit->nom = $request->input('pr_nom');			 
			$produit->save();
			$data["produit_id"] = $produit->id;
			$data["description"] = $produit->description;
			$data["lien"] = $produit->lien;
			$data["nom"] = $produit->nom;
			$data["quantite"] = $produit->quantite;
			
		}
		  $data["message"] = "Les données ont été enregistrées avec succès" ;
            
		  return response()->json($data,200);
	}
	public function addToCommande(Request $request , $id)
	{
    $commande = new Commandes();
		$commande = Commandes::find($id);
		$colis = new Colis();
		$details_commande = $commande->details_commande;
		//dump($details_commande);
		$colis = $details_commande->colis[0]; 
		//dump($colis->id);
		//$commande->details_commande->colis[0];
		$produit = new Produits();
    $produit->colis_id = $colis->id;
		$produit->nom = $request->input('nom');
		$produit->lien = $request->input('lien');
		$produit->description = $request->input('description');
    $produit->quantite = $request->input('quantite');
		$produit->colis_id = $colis->id;
        $produit->save();
        //dump(count($commande->details_commande->colis[0]->produits));
		//$commande->details_commande->colis[0]->produits->push($produit);
         //dump($commande->details_commande->colis[0]->produits);        
        //$commande->save();

    $contenu ='<li class="list-group-item">

                                        <div class="item">
                                        <div class="accordion-header produit'.$produit->id.'">
                                                <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#accordion1a'.count($commande->details_commande->colis[0]->produits).'" aria-expanded="false">
                                                <strong>Article '.count($commande->details_commande->colis[0]->produits).'</strong> : <span class="nom'.$produit->id.'">'.$produit->nom.'</span> | <strong> Qte </strong> : <span id="quantite'.count($commande->details_commande->colis[0]->produits).'" class="quantite'.$produit->id.'">'.$produit->quantite.'</span> 
                                                </button>
                                        </div>

                                            <div id="accordion1a'.count($commande->details_commande->colis[0]->produits).'" class="accordion-body collapse" data-parent="#accordion01" style="">
                                                <div id="accordionContent'.count($commande->details_commande->colis[0]->produits).'" class="accordion-content">
                                                    <div class="overlay" id="productMsg'.count($commande->details_commande->colis[0]->produits).'">
                                                    </div>

                                                    <form>
                                                    
                                                        <div class="card">
                                                          <div class="card-body">
                                                            <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                                                                <p class="text-left col-md-3">
                                                                    <strong>Description</strong>
                                                                </p>
                                                            <p class="d-flex flex-column text-justify description'.$produit->id.'">
                                                                    '.$produit->description.'
                                                                </p>
                                                            </div>
                                                            <!-- /.d-flex -->

                                                            <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                                                                <p class="text-left col-md-3">
                                                                    <strong>Prix unitaire</strong>
                                                                </p>
                                                                <p class="d-flex flex-column text-right col-md-6">
																																				<input type="number" id="montant'.count($commande->details_commande->colis[0]->produits).'" class="form-control" value="0">
																																</p>
                                                                <p class="d-flex flex-column col-md-3">
                                                                    F CFA
                                                                </p>
                                                            </div>
                                                            <!-- /.d-flex -->

                                                            <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                                                                <p class="text-left col-md-3">
                                                                    <strong>Poids ou volume</strong>
                                                                </p>
                                                                <p class="d-flex flex-column text-right col-md-6">
                                                                    <input type="number" id="poids'.count($commande->details_commande->colis[0]->produits).'" class="form-control">
                                                                </p>
                                                                <p class="d-flex flex-column col-md-3">
                                                                    kg ou m3
                                                                </p>
                                                            </div>
                                                            <!-- /.d-flex -->

                                                            <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                                                                <p class="text-left col-md-3">
                                                                    <strong>Statut</strong>
                                                                </p>
                                                                <p class="d-flex flex-column text-right col-md-6 ">
                                                                    <select class="form-control custom-select" id="statut'.count($commande->details_commande->colis[0]->produits).'">
                                                                                                                                                    <option value="DISPONIBLE"> DISPONIBLE </option>
                                                                                                                                                    
                                                                        <option disabled=""></option>
                                                                    
                                                                        <option value="DISPONIBLE"> DISPONIBLE </option>
                                                                        <option value="NON DISPONIBLE"> NON DISPONIBLE </option>
                                                                            
                                                                    </select>
                                                                </p>
                                                                <p class="d-flex flex-column col-md-3">
                                                                   
                                                                </p>
                                                            </div>
                                                            <!-- /.d-flex -->
                                                            <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                                                                <p class="text-left col-md-4">
                                                                <span><a href="'.$produit->lien.'" class="lien'.$produit->id.'" target="_blank">Voir le produit</a></span>
                                                                </p>
                                                                <p class="d-flex flex-column text-right">
                                                                    <input type="button" value="Enregistrer les modifications" class="btn btn-success" onclick="saveProduct('.$produit->id.','.count($commande->details_commande->colis[0]->produits).','.count($commande->details_commande->colis[0]->produits).', '.$commande->id.')">
                                                                </p><br>
                                                                
                                                            </div>
                                                        <button data-produitid="'.$produit->id.'" data-quantite="'.$produit->quantite.'" data-lien="'.$produit->lien.'" data-description="'.$produit->description.'" data-nom="'.$produit->nom.'" data-toggle="modal" data-target="#modal-default" type="button" class="btn  btn-outline-primary btn-lg float-right edit-product">Modifier larticle</button>
                                                          </div>
                                                          
                                                        </div>
                                                    </form>
                                                       
                                                </div>
                                            </div>
                                            
                                        </div>
                                       
                                    </li>';
		                return response($contenu);
		
	}


}
