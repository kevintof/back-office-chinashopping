<?php

namespace App\Http\Controllers\User\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Commandes;
use App\MessageClient;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {

    	$users=User::All();

        $commandeEnCoursDeCotations = Commandes::All()
            ->where('statut_id', 1);

        $commandeEnCoursExecutions = Commandes::All()
            ->where('statut_id', 2);


        $commandeEnAttentes = Commandes::All()
            ->where('statut_id', 3);

        $commandeAnnulees = Commandes::All()
            ->where('statut_id', 25);

        $commandeTerminees = Commandes::All()
            ->where('statut_id', 6);

        $derniereCommandeEnCoursDeCotations= Commandes::orderBy('id','desc')
        ->where('statut_id', 1)->get();

	   $messages = MessageClient::select(MessageClient::raw('distinct client_id, max(id) as maxi'))
	   ->groupBy('client_id')
	   ->orderByDesc('maxi')
	   ->get();

	     $index=0;
	         foreach($messages as $message)
	         {
	             $message = MessageClient::find($message->maxi);
	             $messages[$index] = $message;
	             $index++;
	         }

	      $derniersUtilisateurs=User::orderBy('id','desc')->get();
     
        return view('backend.home.index', compact('users','commandeEnCoursDeCotations','commandeEnCoursExecutions','commandeEnAttentes','commandeAnnulees','commandeTerminees','derniereCommandeEnCoursDeCotations','messages','derniersUtilisateurs'));

       
    }


}
