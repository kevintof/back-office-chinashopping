<?php

namespace App\Http\Controllers\User\Admin;

use App\Colis;
use App\Produits;
use App\Commandes;
use App\Lots;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;


class ColisController extends Controller
{
    public function update(Request $request, $arg, $id_colis){
        
        $coli = Colis::find($id_colis);
        $images = json_decode($coli->images);
        
        $montant_livraison;

        if($coli) {

            //$coli->$arg = $request->input('arg');
            $coli->save();

            switch ($arg) {
                case 'images':
                    $this->validate($request, [
                        'filename.*' => 'mimes:jpg,jpeg,png,jpg,gif,svg'
                    ]);

                    $user_id = $coli->details_commande->commande->user_id;
                    $data['user'] = $user_id;
                    $data['host'] = request()->getHttpHost().'/commandes-colis';  
                    
                    $images = json_decode($coli->images);
                    //dd($images);
                    if($request->hasfile('files'))
                    {

                        foreach($request->file('files') as $file)
                        {
                            
                            $name=$file->getClientOriginalName();
                      //      dd($images);
                            if($images)
                           {
                               
                                if(is_array($images->name))
                                {

                                    
                                    array_push($images->name,$name); 
                                    $data['name'] = $images->name;    
                                }
                                else{
                                    
                                    $data['name'] =[];
                                    array_push($data['name'],$images->name);
                                    array_push($data['name'],$name);
                                      

                                }
                          }
                          else{
                        //    dd('ff');
                            $data['name'] = [];
                            
                            array_push($data['name'],$name);
                          }
                            
                          

                           
                               
                             

                            $file->move(public_path().'/commandes-colis/'.$user_id, $name);   
                        }
                    }
                    
                    $coli->$arg = json_encode($data);
                    $coli->save();                 


                    return response()->json($data, 200);

                    break;
                case 'poids':

                    $coli->$arg = $request->input('arg');
                    $coli->transport = $coli->type_livraison->param5 * $request->input('arg');
                    $coli->save();

                    $commande = Commandes::find($coli->details_commande->commande->id);
                    $commande->montant_livraison = $coli->transport + $coli->frais_livraison;
                    $commande->save();

                    $frais_livraison = 0;
                    if ($coli->frais_livraison != null) {
                        $frais_livraison = $coli->frais_livraison;
                    }


                    return response()->json(['message' => 'Données enregistrées avec succès', 'transport' => $coli->transport, 'frais_livraison' => $frais_livraison], 200);

                    break;
                case 'frais_livraison':

                    $coli->frais_livraison = $request->input('arg');
                    $coli->save();

                    $commande = Commandes::find($coli->details_commande->commande->id);
                    $commande->montant_livraison = $coli->transport + $coli->frais_livraison;
                    $commande->save();

                    return response()->json(['message' => 'Données enregistrées avec succès'], 200);
                    break;

                default:
                   
                    break;
            }   
        }
    }


   // ajout du details de la livraison
   public function createDetails(Request $request){


        if ($request->all == 1) {

            $lot = Lots::find($request->colis_id);

            foreach ($lot->colis as $coli) {

                $date= Carbon::now();

                $data = array();
                $data['position']= $request->position;
                $data['commentaire']= $request->commentaire;
                $data['date'] = $date->toDateTimeString();
                $data['colis_id']= $coli->id;
                   
                $lot= DB::table('livraison_details')->insert($data); 
            }

            
        }else{

            $date= Carbon::now();

            $data = array();
            $data['position']= $request->position;
            $data['commentaire']= $request->commentaire;
            $data['date'] = $date->toDateTimeString();
            $data['colis_id']= $request->colis_id;
               
            $lot= DB::table('livraison_details')->insert($data);        
        }

        return back()->with('success', 'Opération effectuée avec succès');

    }
    // liste des produits par colis
    public function listProduct($id){

        $produits = Produits::where('colis_id', $id)->get();  
        return view('backend.colis.listproduct',['produits' => $produits]);

    }
    
}
