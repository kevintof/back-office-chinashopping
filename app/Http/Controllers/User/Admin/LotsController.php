<?php

namespace App\Http\Controllers\User\Admin;

use App\Lots;
use App\Colis;
use App\Automation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class LotsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $lots = Lots::all();
        $colis= Colis::all()->where('lots',null);
     
        return view('backend.lots.index',['lots' => $lots, 'colis' => $colis]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){ 

        $etat_lots = DB::table('automations')
           ->where('group', '=', 'STATUT')
           ->where('desc', '=', 'LOT')
           ->get();

        return view('backend.lots.create',['etat_lots' => $etat_lots]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
 
        $data = array();
        $data['reference']= $this->make_random_custom_string(6);
        $data['date_depart']= $request->date_depart;
        $data['date_arrive'] = $request->date_arrive;
        $data['statut_id']= $request->statut_id;
            
        $lot = DB::table('lots')->insert($data);      
        return redirect()->route('admin.lots.index')->with('success','Opération effectuée avec succès');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){

        $colis= Colis::all()->where('lots_id',$id);
        $lot = Lots::find($id);
        return view('backend.colis.index', ['colis' => $colis, 'lot_reference' => $lot->reference]);
    }

          /**
     * listes n'appartenant pas à un lot
     * lists not belonging to a lot
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function add($id){
    
        $colis= DB::table('colis')
          ->orderby('id','desc')
          ->where('lots_id','Null')
          ->get();
    
        return view('backend.lots.create',['colis' => $colis]);
    }



    public function send(Request $request){

        $data['lots_id']= $request->lots_id;

        $data = Colis::where('id', '=', $request->colis_id)
          ->update(['lots_id' =>$data['lots_id']]);
        
        return back()->with('success', 'Opération effectuée avec succès');
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $lot = Lots::find($id);

        $etats_lots = Automation::where('group', '=', 'STATUT')
            ->where('desc', '=', 'LOT')
            ->get();
       
        return view('backend.lots.update',['lot' => $lot, 'etats_lots' => $etats_lots]);        
    }



    public function Insert(Request $request){

        $data = array();
        $data['reference']= $request->reference;
        $data['date_depart']= $request->date_depart;
        $data['date_arrive'] = $request->date_arrive;
        $data['statut_id']= $request->statut_id;
       
        $lots = Lots::where('reference',$data['reference'])->update($data);
       
        return redirect()->route('admin.lots.index')->with('success','Opération effectuée avec succès');
    }



    public function delete($id){
        
        DB::table('lots')->delete($id);
        return redirect()->back();
    }



    public function make_random_custom_string($n){

        $alphabet = "1234567890abcdefghijklmnopqrstuvwxyz";
        $s = "";

        for ($i = 0; $i != $n; ++$i)
            $s .= $alphabet[mt_rand(0, strlen($alphabet) - 1)];

        return $s;
    }

   
}
