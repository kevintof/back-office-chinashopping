<?php

namespace App\Http\Controllers\User\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Articles;
use App\Lib\PusherFactory;

class NewsController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth:admin');
    // }

    public function index()
    {
        return view('backend.home.index');
    }

    public function store(Request $request)
    {
        $article = new Articles();
        
        $article->titre = $request->input('titre');

        $article->contenu = $request->input('contenu');
        
        $article->type_id =  $request->input('type_id');
        /*$articleType = Auvetomations::find($request->input('type_id'));

        if($articleType )*/

        if($request->hasfile('photo_avant') )
        {
            $name= $request->file('photo_avant')->getClientOriginalName();
            $data['name'] = $name; 

            $request->file('photo_avant')->move(public_path().'/articles-photos', $name);
            $data['host'] = request()->getHttpHost().'/articles-photos';
            $article->photo_avant = json_encode($data);
        }

        if($request->hasfile('background'))
        {
            $name= $request->file('background')->getClientOriginalName();
            $data['name'] = $name; 

            $request->file('background')->move(public_path().'/articles-photos', $name);
            $data['host'] = request()->getHttpHost().'/articles-photos';
            $article->background = json_encode($data);
        }
        $article->save();
        $donnees['titre_article'] = $request->input('titre');
        $donnees['article_id'] = $article->id;
        $donnees['event-source'] = 'promo';
        //dd($article->type);
        $donnees['type'] = $article->type->param1;
        $pusher = PusherFactory::make();
        $pusher->trigger('tarzan-express', 'promo', $donnees);
        



        return Response($article);



    }

    
}
