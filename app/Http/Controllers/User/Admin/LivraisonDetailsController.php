<?php

namespace App\Http\Controllers\User\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Lib\PusherFactory;

use App\LivraisonDetails;
use App\Colis;

class LivraisonDetailsController extends Controller
{

    public function store(Request $request)
    {
        $details = new LivraisonDetails();
        $details->position = $request->input('position');
        $details->commentaire = $request->input('comentaire');
        $details->date = date("d/m/Y H:i:s");
        $details->colis_id = $request->input('colis_id'); 
        $data['event-source'] = "position_colis";
        $data['user_id'] = $details->colis->details_commande->commande->user_id;
        $data['commande_id'] = $details->colis->details_commande->commande->id;
        $data['commande_reference'] = $details->colis->details_commande->commande->reference;
        $data['colis_reference'] = $details->colis->reference;
        $data['message'] = 'le colis de la commande  '.$details->colis->details_commande->commande->reference.' a changé de position. Veuillez vérifier';

        $pusher = PusherFactory::make();
        $pusher->trigger('tarzan-express','position_colis',$data);

        $details->save();
        return response()->json(['message' => 'Données enregistrées avec succès'], 200);
    }


    public function update(Request $request, $id_details)
    {
        $details = LivraisonDetails::find($id_details);

        $details->position = $request->input('position');
        $details->commentaire = $request->input('comentaire');
        $details->date = date("d/m/Y H:i:s");

        
        $details->save();

        return response()->json(['message' => 'Données enregistrées avec succès'], 200);
    }

    public function getLivraisonDerailsByColi($coli_id)
    {
        $colis = Colis::find($coli_id);
        $details = '';

        foreach ($colis->livraison_details as $livraison_detail) {
            
            $details .= '<div class="col-md-12">
                <!-- The time line -->
                <div class="timeline">
                    <!-- timeline time label -->
                    <div class="time-label">
                        <span class="bg-red">'.$livraison_detail->date.'</span>
                    </div>
                    <!-- /.timeline-label -->

                    <!-- timeline item -->
                    <div>
                        <i class="fas fa-location bg-blue"></i>
                        <div class="timeline-item">
                            <span class="time"><i class="fas fa-clock"></i> 12:05</span>
                            <h3 class="timeline-header">'.$livraison_detail->position.'</h3>

                            <div class="timeline-body">
                                '.$livraison_detail->commentaire.'
                            </div>

                            <div class="timeline-footer">
                                <a class="btn btn-success btn-sm">Modifier</a>
                                <a class="btn btn-danger btn-sm">Supprimer</a>
                            </div>
                        </div>
                    </div>
                    <!-- END timeline item -->
                    <!-- timeline item -->
                </div>
            </div>';
        }

        return response()->json(compact('details'), 200);
    }
}
