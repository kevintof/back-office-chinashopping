<?php

namespace App\Http\Controllers\User\Admin;

use App\User;
use App\Commandes;

use App\CodeAffiliation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index(){

        $users = User::orderBy('id', 'desc')->get();
        return view('backend.users.index', compact('etatCommandes', 'users'));
    }

    public function show_user_affilies($u_id){
        $user = User::find($u_id);
        $code_parrainage= CodeAffiliation::select('id')->where('user_id','=',$u_id);
        $users = User::whereIn('users.code_parrainage_id',$code_parrainage)->get();
       
        $index=0;
        foreach ($users as $user){
            $user = User::find($user->id);
            $users[$index] = $user;
            $index ++ ; 
        }
        
        return view('backend.users.indexAffilies', compact('users','user'));     
    }


    public function show($u_id){

        $users = User::where('id', $u_id)->get();

        $code_parrainage= CodeAffiliation::select('id')->where('user_id','=',$u_id);
        $affilies = User::whereIn('users.code_parrainage_id',$code_parrainage)->get();

        $commandes = Commandes::where('user_id', $u_id)
            ->orderBy('id', 'desc')->get();

        $commandeEnCoursDeCotations = Commandes::where('user_id', $u_id)
            ->where('statut_id', 1)
            ->get();

        $commandeEnCoursExecutions = Commandes::where('user_id', $u_id)
            ->where('statut_id', 2)
            ->get();
       
        $commandeEnAttentes = Commandes::where('user_id', $u_id)
            ->where('statut_id', 3)
            ->get();

        $commandeAnnulees = Commandes::where('user_id', $u_id)
            ->where('statut_id', 25)
            ->get();

        $commandeTerminees = Commandes::where('user_id', $u_id)
            ->where('statut_id', 6)
            ->get();
            
        return view('backend.users.show',compact('users','affilies','commandes',
        'commandeEnCoursDeCotations','commandeEnCoursExecutions','commandeEnAttentes',
        'commandeAnnulees','commandeTerminees'
        ));
    }


}
