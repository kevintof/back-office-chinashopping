<?php

namespace App\Http\Controllers\User\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\NotificationAdmin;
use App\Message;
use App\MessageClient;
class NotificationController extends Controller
{
    public function index()
    {
        return view('backend.home.index');
    }

    public function getCommandesNotifications()
    {
      $notification = NotificationAdmin::where('commande_id','!=',null)->where('etatLecture','=',false)->count();

      return response($notification, 200)
        ->header('Content-Type', 'text/plain');
    }

    public function updateNotificationState($id){
        
        $notification = NotificationAdmin::where('commande_id','=',$id)->where('type','=','NOUVELLE_COMMANDE')->first();
        
        if($notification != null)
        {
            //dump($notification->etatLecture);
            $notification->etatLecture = true;
            //dump($notification->etatLecture);
            $notification->save();
        }
        

        return response()->json(["statut"=>"success"],200);
    }

    public function headMessagesNotification(){
        $messages = Message::where('is_read_by_admin','=',false)->where('is_admin','=',false)->limit(20)->get();

        
         $contenu = '';
        foreach($messages as $message)
        {
            $contenu .=  '<a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">'
              .
              '<div class="media-body">
                <h3 class="dropdown-item-title">'.
                  $message->commande->client->nom.' ( commande N°'.$message->commande->reference.')'
                  .'<span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">'.$message->contenu.'</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> '.$message->date.'</p>
              </div>
            </div>
            
          </a>
          <div class="dropdown-divider"></div>
          ';

        }

        return response($contenu);
    }

    public function get_head_commande_notification(){
      
      $notifications = NotificationAdmin::where('etatLecture','=',false)->get();
      $contenu = '';
      '<a href="#" class="dropdown-item">
          <i class="fas fa-envelope mr-2"></i> 4 new messages
          <span class="float-right text-muted text-sm">3 mins</span>
        </a>
        <div class="dropdown-divider"></div>';
      foreach($notifications as $notification)
      {
         $contenu .= '<a href="#" class="dropdown-item">
         <i class="fab fa-shopify mr-2"></i><small> 
          nouvelle commande ('.$notification->commande->reference.')</small> <span class="float-right text-muted text-sm">3 mins</span>
       </a>
       <div class="dropdown-divider"></div>';
      }

      return response($contenu);


    }

    public function service_clientele_all_not_read_messages()
    {
      $messages_notifications = MessageClient::where('is_read_by_admin','=',false)->where('is_admin','=',false)->count();
       return response($messages_notifications);
    }

    public function message_client_chat_notification($id)
    {
      $chat_notifications = MessageClient::where('is_read_by_admin','=',false)->where('user_id','=',$id)->count();
      $data["chat_notifications"] = $chat_notifications;
      $data["user_id"] = $id;
      return response()->json($data,200);
    }


}