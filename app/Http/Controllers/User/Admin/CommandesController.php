<?php

namespace App\Http\Controllers\User\Admin;
use App\Lib\PusherFactory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Commandes;
use App\Automation;
use App\Notifications\CommandeStateChanged;
use App\Notification;
use App\Transactions;
use App\NotificationAdmin;
use App\Message;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use App\Repositories\AutomationsRepository;
use Illuminate\Support\Facades\DB;
use App\Notifications\CommandeStatusChanged;

class CommandesController extends Controller
{
    
    
    public function index()
    {

    	$etatCommandes = DB::table('automations')
           ->where('group', '=', 'STATUT')
           ->where('desc', '=', 'COMMANDE')
           ->get();
            //dump(Auth::id());
        $commandes = Commandes::orderBy('id', 'desc')->get();

        return view('backend.commandes.index', compact('etatCommandes', 'commandes'));
    }


    public function all($slug)
    {
    	# code...
    }
   public function getCommandesByState(Request $request){

    $filtres = Commandes::
      $filtres->date_debut = $request->input('date_debut'); 
      $filtres->date_fin = $request->input('date_fin');
      $filtres->statut_id = $request->input('statut_id');      

           

     return view('backend.commandes.index', compact('filtres'));

    }


    public function show($id)
    {
    	$etatCommandes = AutomationsRepository::getEtatCommandes();
        
       
         $paymentTypes = AutomationsRepository::getPaymentTypes();
         // dump($paymentTypes);
         $livraisonTypes = AutomationsRepository::getLivraisonTypes();
        $commande = Commandes::find($id);
        
       
        //dump($commande->details_commande->transaction);
        return view('backend.commandes.show', compact('commande', 'etatCommandes','paymentTypes','livraisonTypes'));
    	# code...
    }

    public function update(Request $request, $arg, $id_commande)
    {

        $commande = Commandes::find($id_commande);
        $notification = new Notification();
        $notification->etatLecture = false;
        $notification->date = date('Y-m-d H:i:s');
        $notification->commande_id = $id_commande ;
         
        $data["message"] = 'Données enregistrées avec succès </br> Les notifications ont été envoyées à l\'utilisateurs';

        if($commande) {
            
            $commande->$arg = $request->input($arg);

            if ($arg == "montant_commande") {

                $formule = Automation::where('group', '=', 'SERVICE')
                                    ->where('desc', '=', 'FRAIS')
                                    ->first();

                if ($request->input($arg) < $formule->param2) 
                    $commande->montant_service = $request->input($arg) * $formule->param1;

                else if ($request->input($arg) <= $formule->param2 && $request->input($arg) > $formule->param4)
                    $commande->montant_service = $request->input($arg) * $formule->param3;

                else
                    $commande->montant_service = $request->input($arg) * $formule->param5;

                $data["montant_service"] = $commande->montant_service;
            }           
           
            $commande->save();
            
            
            if ($arg == "statut_id") {
                $notification->type = "ETAT_COMMANDE";
                 
                $statut = Automation::find($request->input($arg));
                $notification->commande_statut_id = $commande->statut_id;
                $notification->seen = false;
                //$channel = 'push_channel';
                $data["class"] = $statut->param5;
                $data["user_id"] = $commande->user->id;
                $data["message"] = "La commande ".$commande->reference." est ".$commande->statut->param1;
                
                $data["reference"] = $commande->reference;
                $data["commande_id"] = $commande->id;
                $data["event-source"] = 'push';
                $notification->save();
                $pushNotifications = PusherFactory::makeBeam();
                $pusher = PusherFactory::make();
                $pusher->trigger('tarzan-express', 'push', $data);


                $publishResponse = new CommandeStatusChanged($pushNotifications,$commande);
                $publishResponse->publishToUsers();
            }
        }

        return response()->json($data, 200);
    }
     

    public function checkif_commande_is_read_or_not($id){
        $notificationState = 0;
        $notificationState = NotificationAdmin::where('commande_id','=',$id)->where('type','=','NOUVELLE_COMMANDE')->where('etatLecture','=',false)->count();
        $data["state"] = false;
       if($notificationState != 0){
          $data["state"] = true;
        }
        $data["id"] = $id;
        return response()->json($data, 200);
    }

    
     
    public function count_commande_messages_not_read_byAdmin($id){
       
        $messages = Message::where('commande_id','=',$id)->where('is_admin','=',false)->where('is_read_by_admin','=',false)->count();
        //dump($messages);
        $data["messages_non_lus"] = $messages;
        $data["commande_id"] = $id;

        return response()->json($data, 200);

    }

    public function update_commande_messages_readByadmin($id){
        
        $message = DB::table('messages')
                  ->where('commande_id', $id)
                  ->where('is_admin', false)
                  ->where('is_read_by_admin', false)
                  ->update(['is_read_by_admin' => true]);
    
              
        $data["status"] = "success";              
        return response()->json($data, 200);
    }

    public function saveTransaction(Request $request ,$id){
        
        $commande = Commandes::find($id);
        $transaction = new Transactions();

        
        $transaction->montant = $request->input('montant');
        $transaction->mode_paiement_id = $request->input('mode_paiement');
         $transaction->date = $request->input('date');
        
        if($request->input('phone_number'))
        {
            $transaction->phone_number = $request->input('phone_number');
        }
        $payment_type = Automation::find($transaction->mode_paiement_id);
        /* if($payment_type->param1 == "FLOOZ" || $payment_type->param1 =="T-MONEY")
        {
          $transaction->mobile_payement_reference = $request->input('reference'); 
        }
        else{
            $transaction->reference = $request->input('reference');
        } */
        
        $identifier = $commande->client_id.$this->make_random_custom_string(9);
        $transaction->reference = $identifier;
        $transaction->mobile_payement_reference = $request->input('reference');
        $transaction->attempt = 1;
        $transaction->commentaire = $request->input('commentaire');
        $etatPaiement = AutomationsRepository::getPaymentState("Effectué");
        
        $transaction->etat_paiement_id = $etatPaiement->id;
        $transaction->save();

        $commande->details_commande->transaction_id = $transaction->id;
        $commande->details_commande->save();
        $commande->save();
        
        return response("ok");
        
    }

    public function make_random_custom_string($n)
    {

        $alphabet = "1234567890";
        $s = "";
        for ($i = 0; $i != $n; ++$i)
            $s .= $alphabet[mt_rand(0, strlen($alphabet) - 1)];
        return $s;
    }
     
    public function modifierModeLivraison(Request $request,$id){

        $commande = Commandes::find($id);
        //dump($request->input('mode_livraison'));
        $commande->details_commande->colis[0]->type_livraison_id = $request->input('mode_livraison') ;
        $colis = $commande->details_commande->colis[0];
        $colis->save();
        $commande->save();
        $livraison = Automation::find($request->input('mode_livraison'));
        $data["livraison"] = $livraison->param1 ;
        return response($data);
        
    }

    public function basic() {
      
        $data = array('name'=>"Virat Gandhi");
        Mail::send('frontend.informations.mail', $data, function($message) {
        $message->to('garrickervin@gmail.com', 'Tutorials Point')->subject
        ('Laravel HTML Testing Mail');
        $message->from('kevinoustoffa@gmail.com','Virat Gandhi');
      });
      
        return response()->json(["statut"=>"success"],200);
     }

     /**envoyer une notification par mail à l'utilisateur après changement d'état d'une commande */
     public function sendMailNotification($commande){
        
        $data = array('name'=>"Service clientèle chinashopping",
                      'commandeReference'=>$commande->reference,  
                      'statut' => 'stat',
                       'mail' =>  $commande->client->mail,
                      'client' => $commande->client->nom
                    );
        Mail::send('frontend.informations.mail', $data, function($message) {
        $message->to($data['mail'], 'Tutorials Point')->subject
        ('[Finapp] Informations de traitement de commande');
        $message->from('kevinoustoffa@gmail.com','China shopping');
      });
      return "ok";
    }
    
}
