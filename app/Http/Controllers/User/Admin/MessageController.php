<?php

namespace App\Http\Controllers\User\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Colis;
use App\Commandes;
use App\MessageClient;
use Illuminate\Support\Facades\DB;
use illuminate\Support\Collection;

class MessageController extends Controller
{
    
    public function index()
    {
        //$messages = DB::table('message_clients')->distinct('client_id')->max('id');
        $messages = DB::table('message_clients')->select(DB::raw('distinct client_id, max(id) as maxi'))->groupBy('client_id')->orderByDesc('maxi')->get();
        /* dump($messages[0]->maxi); */
         /* $messages =  */
         $index=0;
         foreach($messages as $message)
         {
             $message = MessageClient::find($message->maxi);
             $messages[$index] = $message;
             $index++;
         }
         //dump($messages);
        return view('backend.messages.index',compact('messages'));
    }
    

}
