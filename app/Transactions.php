<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    protected $fillable = [
        'reference',
        'mobile_payement_reference',
        'montant',
        'mode_paiement_id',
        'etat_paiement_id',
        'client_id',
        'phone_number',
        'attempt',
        'commentaire',
        'date'
    ];
    public $timestamps = true;

    public function mode_paiement(){
        return $this->belongsTo('App\Automation');
    }

    public function etat_paiement(){
        return $this->belongsTo('App\Automation');
    }

    public function client(){
        return $this->belongsTo('App\users');
    }
}
