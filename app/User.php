<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom', 'prenoms', 'phone_number', 'email', 'password', 'pays_id', 'code_parrainage_id', 'agree', 'verification'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function commandes(){
        return $this->hasMany('App\Commandes');
    }

    public function pays(){
        return $this->belongsTo('App\Pays');
    }
    
    public function code_affiliation(){
        return $this->hasOne('App\CodeAffiliation');
    }
    
    public function code_parrainage(){
        return $this->belongsTo('App\CodeAffiliation');
    }

    public function routeNotificationFor($channel)
    {
        if($channel === 'PusherPushNotifications'){
            return 'jean';
            
        }

        $class = str_replace('\\', '.', get_class($this));

        return $class.'.'.$this->getKey();
    }
}
