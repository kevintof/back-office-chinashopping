<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        
        'commande_id',
        'is_read',
        'contenu',
        'is_admin',
        'is_read_by_admin',
        'date'
    ];
    public $timestamps = false;
    
    public function commande(){
        return $this->belongsTo('App\Commandes');
    }
    
    

    
}
