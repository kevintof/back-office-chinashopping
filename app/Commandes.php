<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\NotificationAdmin;

class Commandes extends Model
{
    protected $fillable = [
        'reference',
        'user_id',
        'details_commande_id',
        'statut_id',
        'montant_livraison',
        'montant_commande',
        'montant_service',
        'information'
    ];

    public $timestamps = false;

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function details_commande(){
        return $this->belongsTo('App\DetailsCommande');
    }

    public function statut(){
        return $this->belongsTo('App\Automation');
    }

    public function lu_par_administration()
    {
        
        $notificationState = 0;
        $notificationState = NotificationAdmin::where('commande_id','=',$this->id)->where('type','=','NOUVELLE_COMMANDE')->where('etatLecture','=',false)->count();
        $state = true;
        if($notificationState != 0){
           $state = false;
         }
         return $state;
    }

    public function messages_non_lus(){
        $messages = Message::where('commande_id','=',$this->id)->where('is_admin','=',false)->where('is_read_by_admin','=',false)->count();
        
        return $messages;
 
    }
}
