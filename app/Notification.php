<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    //
    protected $fillable = [
        'type',
        'etatLecture',
        'date',
        'commande_id',
        'commande_statut_id',
        'seen'
    ];

    public $timestamps = true;

    public function commande(){
        return $this->belongsTo('App\Commandes');
    }

    public function commande_statut(){
        return $this->belongsTo('App\Automation');
    }
    

}
