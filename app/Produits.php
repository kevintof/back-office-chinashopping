<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produits extends Model
{
    protected $fillable = [
        'nom',
        'description',
        'lien',
        'images',
        'poids',
        'montant',
        'quantite',
        'statut',
        'colis_id',
        'unite_id',
        'details_unite'
    ];

    public $timestamps = true;

    public function colis(){
        return $this->belongsTo('App\Colis');
    }

    public function unite(){
        return $this->belongsTo('App\Automation');
    }
}
