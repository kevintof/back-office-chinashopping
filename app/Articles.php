<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    protected $fillable = [
        'titre',
        'photo_avant',
        'background',
        'contenu',
        'type_id'
    ];

    public $timestamps = true;

    public function type()
    {
        return $this->belongsTo('App\Automation');
    }
}
