<?php
namespace App\Notifications;
use App\Commandes;

class MontantCommandeUpdated extends CommandeNotification
{
  
	public function notifyUser()
	{
		$interest = "user".$this->commande->user->id;
		return $this->beam->publishToInterests(
            ["jean",$interest],
            [
                "apns" => [
                  "aps" => [
                    "alert" => "Hello!",
                  ],
                ],
                "web"=> [
                  "notification" => [
                    "title" => "Tarzan express",
                    "body" => "La commande ".$this->commande->reference."est passé ".$this->commande->statut->param1,
                    "deep_link"=> "http://127.0.0.1:8000/commande/show/".$this->commande->id
                  ],
                ],
                "fcm" => [
                  "notification" => [
                    "title" => "Tarzan express",
                    "body" => "La commande".$this->commande->reference."est passé ".$this->commande->statut->param1,
                    "deep_link" => "https://www.facebook.com"
                  ],
                ],
              ]);
	}
}
   
?>