<?php
  namespace App\Repositories;
  use Illuminate\Support\Facades\DB;
  

  class AutomationsRepository{

    public static function getPaymentTypes(){
       return DB::table('automations')
                    ->where('group','=','TYPE')
                    ->where('desc','=','PAIEMENT')
                    ->get();
                
    } 
    public static function getLivraisonTypes(){
      return DB::table('automations')
                ->where('group','=','TYPE')
                ->where('desc','=','LIVRAISON')
                ->get();
    }

    public static function getEtatCommandes(){
     return DB::table('automations')
            ->where('group', '=', 'STATUT')
            ->where('desc', '=', 'COMMANDE')
            ->get();
    }

    public static function getPaymentState($etat)
    {
      return DB::table('automations')
                   ->where('group','=','STATUT')
                    ->where('desc','=','PAIEMENT')
                    ->where('param1','=',$etat)
                    ->first();
    }
  }