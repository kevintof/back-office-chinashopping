<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationAdmin extends Model
{
    //
    protected $fillable = [
        'type',
        'etatLecture',
        'date',
        'commande_id'
    ];

    public $timestamps = false;

    public function commande(){
        return $this->belongsTo('App\Commandes');
    }
}
