<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lots extends Model
{
    protected $fillable = [
        'reference',
        'date_depart',
        'date_arrive',
        'statut_id'
    ];

    public $timestamps = true;

    public function colis(){
        return $this->hasMany('App\Colis');
    }

    public function statut(){
        return $this->belongsTo('App\Automation');
    }

}
