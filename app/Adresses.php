<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adresses extends Model
{
    protected $fillable = [
        'pays',
        'ville',
        'quartier',
        'adresse1',
        'adresse2'
    ];

    public $timestamps = true;
}
