@extends('backend.layouts.app')

	@section('page')
		utilisateurs
	@endsection

	@section('content')

	    <div class="row mt-3">
	        <div class="col-lg-12">
	            <div class="card">

	                <div class="card-header">

	                    <div class="float-left">
	                        <p class="lead">
	                            <i class="fa fa-users"></i>
	                            <strong> Liste des utilisateurs</strong>
	                        </p>
	                    </div>

	                    <div class="float-right">
	                        <p class="lead">
	                            <strong> Utilisateurs enregistrés : <b>{{ count($users) }}</b> </strong>
	                        </p>
	                    </div>
	                </div>


	                <!-- /.card-header -->

	                <div class="card-body">
	                	
	                    <table id="dataUsers" class="table table-bordered table-striped">
	                        <thead>

	                        <tr>
	                            <th>No</th>
	                            <th>Nom et Prénoms</th>
	                            <th>Numéros de tels</th>
	                            <th>Date inscription</th>
	                            <th>Action</th>
	                        </tr>

	                        </thead>

	                        <tbody>
	                            @php ($i = 0)
	                            @foreach($users as $user)
	                                <tr>

	                                    <th>
	                                        {{ ++$i }}
	                                    </th>
	                                    <th>
	                                        {{ $user->nom }} {{ $user->prenoms }}
	                                    </th>
	                                    <th>
	                                        {{ $user->phone_number }}
	                                    </th>
	                                    <th>
	                                        {{ $user->created_at }}
	                                    </th>
	                                    <th>
	                                        <div class="btn-group">
	                                            <a type="button" class="btn btn-success" href="{{ route('admin.user.show', $user->id) }}" title="Détails"><i class="nav-icon fas fa-eye"></i></a>

	                                            <a type="button" class="btn btn-warning" href="{{ route('admin.user.show.affilies', $user->id) }}" title="Utilisateurs affiliés"><i class="nav-icon fa fa-users"></i></a>

	                                            <a type="button" class="btn btn-info" href="" title="Envoyez une note au client"><i class="fas fa-envelope"></i></a>
	                                            
	                                          </div>
	                                    </th>
	                                </tr>
	                            @endforeach

	                        </tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	    </div>


	@endsection
	@section('javascripts')
	    <script>
	    	$(document).ready(function(){

	            $("#dataUsers").DataTable({
	              "responsive": true,
	              "autoWidth": false,
	            });
			})
	    </script>    
	@endsection
	
