@extends('backend.layouts.app')

	@section('page')
		utilisateurs
	@endsection

	@section('content')

	<section class="content">
      	<div class="container-fluid">
	        <div class="row">
	          	<div class="col-12">

		            <div class="callout callout-info">
		              	<h5><i class="fas fa-info"></i> Note:</h5>
		            	Affichage des informations du client
		            </div>

					<section class="content">
	        			<div class="container-fluid">


				          	<!-- Info boxes -->
				          	<div class="row">

					            <div class="col-12 col-sm-4 col-md-4">
					                <div class="info-box">
					                  	<span class="info-box-icon bg-info elevation-1">
					                  		<i class="fas fa-cog"></i>
					                  	</span>
									  
					                  	<div class="info-box-content">
									
					                    	<span class="info-box-text">Com. en cours de cotation</span>
										
					                    	<span class="info-box-number">
												{{count($commandeEnCoursDeCotations)}}
					                    	</span>
									
					                  	</div>
					                </div>
					            </div>
					          


					            <div class="col-12 col-sm-4 col-md-4">
					                <div class="info-box">
					                  	<span class="info-box-icon bg-success elevation-1">
					                  		<i class="fas fa-spinner"></i>
					                  	</span>

					                  	<div class="info-box-content">
					                    	<span class="info-box-text">Com. en cours d'exécution</span>
					                    	<span class="info-box-number">	{{count($commandeEnCoursExecutions)}}</span>
					                  	</div>
					                  <!-- /.info-box-content -->
					                </div>
					                <!-- /.info-box -->
					            </div>


					            <div class="col-12 col-sm-4 col-md-4">
					                <div class="info-box">
					                  	<span class="info-box-icon bg-secondary elevation-1"><i class="fas fa-pause"></i></span>

					                  	<div class="info-box-content">
					                    	<span class="info-box-text"> Com. en attente de paiement </span>
										  	<span class="info-box-number">	{{count($commandeEnAttentes)}} </span>
					                  	</div>
					                </div>
					            </div>
					       

					            <div class="col-12 col-sm-4 col-md-4">
					                <div class="info-box">
					                  	<span class="info-box-icon bg-danger elevation-1">
					                  		<i class="fas fa-exclamation-circle"></i>
					                  	</span>

						                <div class="info-box-content">
						                    <span class="info-box-text">Com. Annulées</span>
						                    <span class="info-box-number">{{count($commandeAnnulees)}}</span>
						                </div>
					              
					                </div>  
					            </div>

					            
								<div class="col-12 col-sm-4 col-md-4">
					                <div class="info-box">
					                  	<span class="info-box-icon bg-success elevation-1">
					                  		<i class="fas fa-check-square"></i>
					                  	</span>

					                  	<div class="info-box-content">
					                    	<span class="info-box-text">Com. Terminées</span>
					                    	<span class="info-box-number">{{count($commandeTerminees)}}</span>
					                  	</div>
					                </div>
					            </div>

					            <div class="col-12 col-sm-4 col-md-4">
				                <div class="info-box">
				                  	<span class="info-box-icon bg-info elevation-1">
				                  		<i class="fas fa-dollar-sign"></i>
				                  	</span>

				                  	<div class="info-box-content">
				                    	<span class="info-box-text"> Transactions effectuées </span>
				                    	<span class="info-box-number">{{count($commandeTerminees)}}</span>
				                  	</div>
				                </div>
				            	</div>
				            
				          	</div>
	        			</div>
	      			</section>

		  			<section>
		  				@foreach($users as $user)
		                            
						  	<div class="row">
					          	<div class="col-md-4">
					            	<div class="card card-widget widget-user-2">

										<div class="widget-user-header bg-secondary">
											<div class="widget-user-image">
												<img class="img-circle elevation-2" src="{{ asset('admin/dist/img/user2-160x160.jpg') }}" alt="User Avatar">
											</div>
												<!-- /.widget-user-image -->
				                          
											<h3 class="widget-user-username">
												{{ $user->nom }} {{ $user->prenoms }}
											</h3>

											<h5 class="widget-user-desc">
												{{ $user->created_at }}
											</h5>
										</div>

										<div class="card-footer p-0">
											<ul class="nav flex-column">
												<li class="nav-item">
													<a href="#" class="nav-link">
													Numéro de téléphone <span class="float-right badge bg-primary"> +{{ $user->pays->indicatif }}  {{ $user->phone_number }}</span>
													</a>
												</li>
												<li class="nav-item">
													<a href="#" class="nav-link">
													Email <span class="float-right badge bg-primary">   {{ $user->email }}</span>
													</a>
												</li>
												<li class="nav-item">
													<a href="#" class="nav-link">
													Pays <span class="float-right badge bg-primary">   {{ $user->pays->nom }}</span>
													</a>
												</li>
												<li class="nav-item">
											
													<a href="{{ route('admin.user.show.affilies', $user->id) }}" class="nav-link">
													Code parrainage <span class="float-right badge bg-primary">  {{ $user->code_affiliation->code}}</span>
													</a>
												</li>
												<li class="nav-item">
											
													<a href="{{ route('admin.user.show.affilies', $user->id) }}" class="nav-link">
													Nombre des affiliés <span class="float-right badge bg-primary">  {{ count($affilies)}}</span>
													</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
									
								<div class="card-body col-md-8 card">
												
									<center><strong>LISTES DES AFFILIES</strong></center>
									<table id="listAffilies" class="table table-bordered table-striped">
										<thead>
											<tr>
												<th> No </th>
												<th> Nom et Prénoms </th>
												<th> Téléphone </th>
												<th> Code parrainage </th>
												<th> Commande </th>
											</tr>
										</thead>

										<tbody>
											@php ($i = 0)
											@foreach($affilies as $affilie)
												<tr>
													<th>
														{{ ++$i }}
													</th>
													<th>
														{{ $affilie->nom }} {{ $affilie->prenoms }}
													</th>
													<th>
														+{{ $affilie->pays->indicatif }} {{ $affilie->phone_number }}
													</th>
													<th>
														{{ $affilie->code_affiliation->code }}
													</th>
													<th>
														{{ count($user->commandes) }} 
													</th>
																					
												</tr>
											@endforeach
										</tbody>
									</table>							
								</div>			
							</div>

						@endforeach
		  			</section>

				  	<section>
				  		<div class="row mt-3">
				            <div class="col-lg-12">
				                <div class="card">

				                    <div class="card-header">

				                        <div class="float-left">
				                            <p class="lead">
				                                <i class="fab fa-shopify"></i>
				                                <strong> Liste des commandes</strong>
				                            </p>
				                        </div>

				                        <div class="float-right">
				                            <p class="lead">
				                            </p>
				                        </div>
				                    </div>

					                <div class="card-body">
					                    
					                    <div id="user_model_details"></div>
					                    <table id="listCommande" class="table table-bordered table-striped">
					                        <thead>
						                        <tr>
						                            <th>No</th>
						                            <th>Reference</th>
						                            <th>Etat</th>
						                            <th>Date</th>
						                            <th>Action</th>
						                        </tr>
					                        </thead>

					                        <tbody>
												@php ($i = 0)
												@foreach($commandes as $commande)
													<tr>
														<th>
															{{ ++$i }}
														</th>
														<th>
															{{ $commande->reference }}
														</th>
														<th>
															{{ $commande->statut->param1 }}
														</th>
														<th>
															{{ $commande->details_commande->date_commande }}
														</th>
														<th>
															<div class="btn-group">
					                                            <a type="button" class="btn btn-success" href="{{ route('admin.commande.show', $commande->id) }}" title="Détails"><i class="nav-icon fas fa-eye"></i></a>

					                                          </div>
														</th>
													</tr>
												 @endforeach
					                        </tbody>
					                    </table>
					                </div>
				                </div>
				            </div>
			        	</div>
				  	</section>

	          	</div><!-- /.col -->
	        </div><!-- /.row -->
      	</div><!-- /.container-fluid -->
    </section>

	@endsection
	@section('javascripts')
	    <script>
	    	$(document).ready(function(){

	            $("#listAffilies, #listCommande").DataTable({
	              "responsive": true,
	              "autoWidth": false,
	            });
			})
			
	    </script>    
		
	@endsection
	
