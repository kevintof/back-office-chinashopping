@extends('backend.layouts.app')

	@section('page')
		utilisateurs
	@endsection

	@section('content')

	    <div class="row mt-3">
	        <div class="col-lg-12">
	            <div class="card">

	                <div class="card-header">

	                    <div class="float-left">
	                        <p class="lead">
								
	                            <i class="fa fa-users"></i>
	                            <strong>{{$user->nom}} {{$user->prenoms}} </strong> | Liste des parrainés
	                        </p>
	                    </div>
	                </div>


	                <!-- /.card-header -->

	                <div class="card-body">

	                    <table id="listAffilies" class="table table-bordered table-striped">
	                        <thead>

	                        <tr>
	                            <th> No </th>
	                            <th> Nom et Prénoms </th>
	                            <th> Numéros de tels </th>
	                            <th> Code d'affiliation </th>
	                            <th> Nbre Commande </th>
	                            <th> Action </th>
	                          
	                        </tr>

	                        </thead>

	                        <tbody>
	                            @php ($i = 0)

		                        @foreach($users as $user)
			                        <tr>
			                            <th> {{ ++$i }}</th>
			                            <th> {{ $user->nom }} {{ $user->prenoms }} </th>
			                            <th> +{{ $user->pays->indicatif }} {{ $user->phone_number }} </th>
			                            <th> {{ $user->code_affiliation->code }} </th>
			                            <th> {{ count($user->commandes) }} </th>
			                            <th> <a type="button" class="btn btn-info" href=""><i class="fas fa-dollar-sign"></i> Voir chiffre affaire <i class="fas fa-dollar-sign"></i></a> </th>
			                            
			                        </tr>

								@endforeach
	                         
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	    </div>
	@endsection


	@section('javascripts')
	    <script>
			$(document).ready(function(){

	            $("#listAffilies").DataTable({
	              "responsive": true,
	              "autoWidth": false,
	            });
			})

	    </script>    
	@endsection