<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="{{ __('home') }}" class="brand-link">
    <img src="{{ asset('admin/dist/img/logo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
         style="opacity: .8">
    <span class="brand-text font-weight-light">TARZAN EXPRESS</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{ asset('admin/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="{{ __('home') }}" class="d-block">TARZAN ADMIN</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        
        <li class="nav-item">
          <a href="{{ route('admin.commandes') }}" class="nav-link">
            <i class="fab fa-shopify"></i>
            <p>
              Commandes
              <span class="right badge badge-danger commandes"></span>
            </p>
          </a>
        </li>

        <li class="nav-item">
          <a href="{{ route('admin.users.index') }}" class="nav-link">
            <i class="fas fa-users"></i>
            <p>
              Utilisateurs
              <span class="right badge badge-danger"></span>
            </p>
          </a>
        </li>

        <li class="nav-item">
        <a href="{{ route('admin.lots.index') }}" class="nav-link">
              <i class="fa fa-cubes"></i>
            <p>
              Gestion des colis / Lots
              <span class="right badge badge-danger "></span>
            </p>
          </a>
        </li>

        <li class="nav-item">
          <a href="{{ route('admin.ges-messages') }}" class="nav-link">
            <i class="fas fa-envelope"></i>
            <p>
              Messages
              <span class="right badge badge-danger messages"></span>
            </p>
          </a>
        </li>

        <li class="nav-item">
          <a href="{{ route('admin.parametres') }}" class="nav-link">
            <i class="fas fa-tools"></i>
            <p>
              Paramètres
            </p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>