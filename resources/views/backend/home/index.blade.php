@extends('backend.layouts.app')

	@section('page')
		dashbord
	@endsection

	@section('content')

		    <!-- Info boxes -->
          <div class="row">
              <div class="col-12 col-sm-4 col-md-4s">
                <div class="info-box">
                  <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

                  <div class="info-box-content">
                    <a href="{{ route('admin.users.index') }}">
                    <span class="info-box-text">Utilisateurs</span>
                    <span class="info-box-number">
                     {{count($users)}}
                    
                    </span>
                    </a>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
              <!-- /.col -->


              <div class="col-12 col-sm-4 col-md-4">
                <div class="info-box mb-3">
                  <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cog"></i></span>

                  <div class="info-box-content">
                    <a href=" {{ route('admin.commandes') }}">
                    <span class="info-box-text">Com. en cotation</span>
                    <span class="info-box-number"> {{count($commandeEnCoursDeCotations)}}</span>
                    </a>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
              <!-- /.col -->

              <!-- fix for small devices only -->
              <div class="clearfix hidden-md-up"></div>

              <div class="col-12 col-sm-4 col-md-4">
                <div class="info-box mb-3">
                  <span class="info-box-icon bg-success elevation-1"><i class="fas fa-spinner"></i></span>

                  <div class="info-box-content">
                    <a href="{{ route('admin.commandes') }}">
                    <span class="info-box-text">Com. en cours <br> d'exécution</span>
                    <span class="info-box-number">{{count($commandeEnCoursExecutions)}}</span>
                    </a>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
              <!-- /.col -->

              <div class="col-12 col-sm-4 col-md-4">
                <div class="info-box mb-3">
                  <span class="info-box-icon bg-secondary elevation-1"><i class="fas fa-pause"></i></span>

                  <div class="info-box-content">
                    <a href="{{ route('admin.commandes') }}">
                    <span class="info-box-text">Com. en attente <br> de paiement  </span>
                    <span class="info-box-number">{{count($commandeEnAttentes)}}</span>
                    </a>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
              <!-- /.col -->


                      <div class="col-12 col-sm-4 col-md-4">
                          <div class="info-box">
                              <span class="info-box-icon bg-danger elevation-1">
                                <i class="fas fa-exclamation-circle"></i>
                              </span>
                                <a href="{{ route('admin.commandes') }}">
                            <div class="info-box-content">
                                <span class="info-box-text">Com. Annulées</span>
                                <span class="info-box-number">{{count($commandeAnnulees)}}</span>
                            </div>
                        </a>
                          </div>  
                      </div>

                      
                <div class="col-12 col-sm-4 col-md-4">
                          <div class="info-box">
                              <span class="info-box-icon bg-success elevation-1">
                                <i class="fas fa-check-square"></i>
                              </span>
                                <a href="{{ route('admin.commandes') }}">
                              <div class="info-box-content">
                                <span class="info-box-text">Com. Terminées</span>
                                <span class="info-box-number">{{count($commandeTerminees)}}</span>
                              </div>
                              </a>
                          </div>
                      </div>
          </div>
        <!-- /.row -->

          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <div class="col-md-8">
              
              

              <!-- TABLE: LATEST ORDERS -->
              <div class="card">
                <div class="card-header border-transparent">
                  <h3 class="card-title">Listes des dernières commandes en cours de cotation</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                      <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                      <i class="fas fa-times"></i>
                    </button>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body ">
                  <div class="table-responsive">

                    <table id="listCommande" class="table table-bordered table-striped">
                      <thead>

                      <tr>
                        <th>Référence</th>
                        <th>Utilisateurs</th>
                        <th>Date</th>
                        <th>Action</th>
                      </tr>
                      </thead>
                      <tbody>
                       
                      @foreach($derniereCommandeEnCoursDeCotations as $derniereCommandeEnCoursDeCotation)
                      <tr>
                        <th>
                            {{ $derniereCommandeEnCoursDeCotation->reference }} 
                          </th>
                          <th>
                            <a href="{{ route('admin.user.show', $derniereCommandeEnCoursDeCotation->user->id) }}">
                          {{ $derniereCommandeEnCoursDeCotation->user->nom }} {{ $derniereCommandeEnCoursDeCotation->user->prenoms }} 
                        </a>
                          </th>
                          <th>
                            {{ $derniereCommandeEnCoursDeCotation->details_commande->date_commande }}
                          </th>
                          <th>
                            <div class="btn-group">
                                            <a type="button" class="btn btn-success" href="{{ route('admin.commande.show', $derniereCommandeEnCoursDeCotation->id) }}" title="Détails"><i class="nav-icon fas fa-eye"></i></a>
                                            
                                            
                                          </div>
                          </th>
                      </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>
            
                </div>
               
              </div>
              
            </div>
            

            <div class="col-md-4">
             
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Derniers messages</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                      <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                      <i class="fas fa-times"></i>
                    </button>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                  <ul class="products-list product-list-in-card pl-2 pr-2">
                  @foreach($messages as $message)
                    <li class="item">
                      <div class="product-img">
                      <img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img">
                      <span class="online_icon"></span> </div>
                      <div class="product-info">
                       {{$message->client->nom}} 
                       
                        <span class="product-description">
                        {{$message->contenu}}
                        </span>
                      </div>
                    </li>
                 @endforeach
                  </ul>
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-center">
                  <a href="{{ route('admin.ges-messages') }}" class="uppercase">Voir les messages</a>
                </div>
                <!-- /.card-footer -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->


          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Listes des derniers Utilisateur inscrit </h3>
                </div>
                <!-- /.card-header -->
               <div class="card-body ">
                  <div class="table-responsive">

                    <table id="listCommande" class="table table-bordered table-striped">
                      <thead>

                      <tr>
                        <th>Nom</th>
                        <th>Numéro</th>
                        <th>Date</th>
                        <th>Action</th>
                      </tr>
                      </thead>
                      <tbody>
                       
                      @foreach($derniersUtilisateurs as $derniersUtilisateur)
                      <tr>
                        <th>
                           {{ $derniersUtilisateur->nom }}
                           {{ $derniersUtilisateur->prenoms }} 
                       </th>
                           
                           
                          <th>
                             +{{ $derniersUtilisateur->pays->indicatif }}{{ $derniersUtilisateur->phone_number}}
                          </th>
                          <th>
                           {{ $derniersUtilisateur->created_at }}
                         </th>
                        
                          <th>
                            <div class="btn-group">
                                            <a type="button" class="btn btn-success" href="{{ route('admin.user.show', $derniersUtilisateur->id) }}" title="Détails"><i class="nav-icon fas fa-eye"></i></a>
                                            
                                          </div>
                          </th>
                      </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>
            
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
	@endsection

  @section('javascripts')
      <script>
        $(document).ready(function(){

              $("#listCommande,listUtilisateur").DataTable({
                "responsive": true,
                "autoWidth": false,
              });
      })
      
      </script>    
    
  @endsection