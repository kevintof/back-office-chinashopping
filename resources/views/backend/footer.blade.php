  <!-- REQUIRED SCRIPTS -->
  <!-- jQuery -->
  <script src="{{ asset('admin/plugins/jquery/jquery.min.js') }}"></script>
  <!-- Bootstrap -->
  <script src="{{ asset('admin/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <!-- overlayScrollbars -->
  <script src="{{ asset('admin/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('admin/dist/js/adminlte.js') }}"></script>

  <!-- OPTIONAL SCRIPTS -->
  <script src="{{ asset('admin/dist/js/demo.js') }}"></script>

  <!-- DataTables -->
  <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  <script src="{{asset('lobibox/dist/js/lobibox.min.js')}}"></script>
 
  <script src="https://js.pusher.com/7.0/pusher.min.js"></script> 
  <script src="https://js.pusher.com/beams/service-worker.js"></script>
  
  
  <script src="https://js.pusher.com/beams/1.0/push-notifications-cdn.js"></script> 
  
  <script src="{{asset('admin/badge_notifications.js')}}"></script>
  <script>
    const beamsClient = new PusherPushNotifications.Client({
      instanceId: '25ee16ac-55dd-4d65-b870-8b4495092d71',
    });

    beamsClient.start()
      .then(() => beamsClient.addDeviceInterest('jean'))
      .then(() => console.log('Successfully registered and subscribed!'))
      .catch(console.error);
  </script>

  <script>
    var pusher = new Pusher('710a42ce02a3f4836392');
    var channel = pusher.subscribe('tarzan-express');
    channel.bind('chat', function(data) {
      
      
      console.log('...some...' )
      if(data.is_admin == false)
      {
        Lobibox.notify('warning', {
                    position: 'top right',
                    title: 'informations', 
                    msg: 'Le client '+data.sender_name+' a envoyé un message par rapport a sa commande',
                    sound: true
                  });
      }   
     
  });
  </script>
  @yield('javascripts')
</body>
</html>