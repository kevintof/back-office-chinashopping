@extends('backend.layouts.app')
    @section('linkCss')
        <link rel="stylesheet" href="{{asset('admin/admin-clientele-chat.css')}}"/>
    @endsection
    @section('page')
		messages
    @endsection

    @section('content')

        <div class="row mt-3">
                <div class="col-lg-12">
                    <div class="card">

                        <div class="card-header">

                            <div class="float-left">
                                <p class="lead">
                                    <i class="fas fa-envelope"></i>
                                    <strong> Messages</strong>
                                </p>
                            </div>
                        </div>


                        <!-- /.card-header -->

                    <div class="card-body">
                        <div class="container-fluid h-100">
                                <div class="row justify-content-center h-100">
                                    <div class="col-md-4 col-xl-3 discussion"><div class="card mb-sm-3 mb-md-0 contacts_card">
                                        <div class="card-header">
                                            <div class="input-group">
                                                <input type="text" placeholder="Search..." name="" class="form-control search">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text search_btn"><i class="fas fa-search"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body contacts_body">
                                            <ul class="contacts">
                                                @foreach($messages as  $message)
                                                    <li class="active">
                                                        <a href="#" class="chat_begin" data-userid="{{$message->client->id}}" data-usernom="{{$message->client->nom}}">
                                                        <div class="d-flex bd-highlight">
                                                            <div class="img_cont">
                                                                <img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img">
                                                                <span class="online_icon"></span>
                                                            </div>
                                                            <div class="user_info">
                                                                <span>{{$message->client->nom}} <!-- &nbsp;&nbsp;&nbsp;<a style="margin-right:0px;" href="#" class="right badge badge-danger">1</a> --> </span>

                                                                <p>{{$message->contenu}}</p>
                                                                
                                                            </div>
                                                            
                                                            
                                                        </div>
                                                       </a>
                                                       
                                                 </li>
                                                @endforeach
                                            
                                            </ul>
                                        </div>
                                        <div class="card-footer"></div>
                                    </div></div>
                                    <div class="col-md-8 col-xl-6 discussion">
                                        <div class="card side-box">
                                           <div>
                                            <img src="{{asset('images/message.svg')}}" style="width:100%;padding:20%"/>
                                           </div> 
                                            <div class="card-body" >
                                                <div style="margin-left:20%;">
                                                   <strong>Veuillez selectionner une discussion</strong>
                                                </div>
                                            </div>
                                            <!-- <div class="card-header msg_head">
                                                <div class="d-flex bd-highlight">
                                                    <div class="img_cont">
                                                        <img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img">
                                                        <span class="online_icon"></span>
                                                    </div>
                                                    <div class="user_info">
                                                        <span>Chat with Khalid</span>
                                                        <p>1767 Messages</p>
                                                    </div>
                                                    <div class="video_cam">
                                                        <span><i class="fas fa-video"></i></span>
                                                        <span><i class="fas fa-phone"></i></span>
                                                    </div>
                                                </div>
                                                <span id="action_menu_btn"><i class="fas fa-ellipsis-v"></i></span>
                                                <div class="action_menu">
                                                    <ul>
                                                        <li><i class="fas fa-user-circle"></i> View profile</li>
                                                        <li><i class="fas fa-users"></i> Add to close friends</li>
                                                        <li><i class="fas fa-plus"></i> Add to group</li>
                                                        <li><i class="fas fa-ban"></i> Block</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="card-body msg_card_body">
                                                <div class="d-flex justify-content-start mb-4">
                                                    <div class="img_cont_msg">
                                                        <img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img_msg">
                                                    </div>
                                                    <div class="msg_cotainer">
                                                        Hi, how are you samim?
                                                        <span class="msg_time">8:40 AM, Today</span>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-end mb-4">
                                                    <div class="msg_cotainer_send">
                                                        Hi Khalid i am good tnx how about you?
                                                        <span class="msg_time_send">8:55 AM, Today</span>
                                                    </div>
                                                    <div class="img_cont_msg">
                                                
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-start mb-4">
                                                    <div class="img_cont_msg">
                                                        <img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img_msg">
                                                    </div>
                                                    <div class="msg_cotainer">
                                                        I am good too, thank you for your chat template
                                                        <span class="msg_time">9:00 AM, Today</span>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-end mb-4">
                                                    <div class="msg_cotainer_send">
                                                        You are welcome
                                                        <span class="msg_time_send">9:05 AM, Today</span>
                                                    </div>
                                                    <div class="img_cont_msg">
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-start mb-4">
                                                    <div class="img_cont_msg">
                                                        <img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img_msg">
                                                    </div>
                                                    <div class="msg_cotainer">
                                                        I am looking for your next templates
                                                        <span class="msg_time">9:07 AM, Today</span>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-end mb-4">
                                                    <div class="msg_cotainer_send">
                                                        Ok, thank you have a good day
                                                        <span class="msg_time_send">9:10 AM, Today</span>
                                                    </div>
                                                    <div class="img_cont_msg">
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-start mb-4">
                                                    <div class="img_cont_msg">
                                                        <img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img_msg">
                                                    </div>
                                                    <div class="msg_cotainer">
                                                        Bye, see you
                                                        <span class="msg_time">9:12 AM, Today</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-footer">
                                                <div class="input-group">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text attach_btn"><i class="fas fa-paperclip"></i></span>
                                                    </div>
                                                    <textarea name="" class="form-control type_msg" placeholder="Type your message..."></textarea>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text send_btn"><i class="fas fa-location-arrow"></i></span>
                                                    </div>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endsection
  @section('javascripts')

  <script>
    $(document).ready(function(){
    $('#action_menu_btn').click(function(){
        $('.action_menu').toggle();
    });

   });
  </script>
    <script src="{{asset('admin/admin-clientele-chat.js')}}"></script>
  @endsection 