@extends('backend.layouts.app')

    @section('button')
        
    @endsection

	@section('page')
		lots
	@endsection



	@section('content')


   <form method="POST" action="{{route('admin.lots.insert')}}" >
        @csrf
    
        <div class="form-group ">
            <div class="form-group ">
                <input type="text" name="reference" class="form-control" value="{{ $lot->reference }}" id="reference" readonly="true">
            </div>

            <div class="form-group ">
                <input type="date" name="date_depart" class="form-control" value="{{ $lot->date_depart }}" id="date_depart" placeholder="date depart">
            </div>
                
            <div class="form-group ">
                <input type="date" name="date_arrive"class="form-control" value="{{ $lot->date_arrive }}" id="date_arrive" placeholder="date arrive">
            </div>

            <div class="form-group">
                
                <select class="form-control" name="statut_id">
                    
                    @foreach($etats_lots as $etat_lots)
                        @if($etat_lots->id == $lot->statut_id)
                            <option value="{{ $etat_lots->id }}" selected>
                                <span class="text-danger">{{ $etat_lots->param1 }}</span>
                            </option>
                        @else
                            <option value="{{ $etat_lots->id }}">
                                <span class="text-danger">{{ $etat_lots->param1 }}</span>
                            </option>
                        @endif
                        
                    @endforeach
                </select>
            
            </div>                
         
        </div>
            
        <button type="submit" name="submit" class="btn btn-primary"> Valider </button>
    </form>

@endsection

