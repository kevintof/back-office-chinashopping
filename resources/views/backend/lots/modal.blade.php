
<!-- colis -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title center" id="exampleModalLabel"> Listes des colis disponibles </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form method="POST" action="{{ route('admin.lots.send') }}" >
          @csrf
          <div class="modal-body">
              <input type="hidden" id="lot_id" name="lots_id" >
          </div>
          <div class="modal-body">
              <div>
                @foreach($colis as $coli)
                  <input type="checkbox" id="scales" value="{{ $coli->id }}" name="colis_id">

                  <label for="colis">
                    {{$coli->reference}} | {{$coli->details_commande->date_commande}}
                  </label> <br>
                @endforeach  
            
              </div>
          </div>
          <button  class="btn btn-success" type="submit">APPLIQUER LES MODIFICATIONS</button>

      </form>

    </div>
  </div>
</div>