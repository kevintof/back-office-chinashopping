@extends('backend.layouts.app')

    @section('button')
        
    @endsection

	@section('page')
		lots
	@endsection



	@section('content')


   <form method="POST" action="{{route('admin.lots.store')}}" >
    @csrf

     <div class="form-group">
          
              
         <div class="form-group ">

                <div class="form-group ">
                    <input type="date" name="date_depart" class="form-control" id="date_depart" placeholder="Date depart">
                </div>
                
                <div class="form-group ">
                    <input type="date" name="date_arrive"class="form-control" id="date_arrive" placeholder="Date prévu arrivée">
                </div>

                <div class="form-group">
                
                    <select class="form-control" name="statut_id" >                                
                        @foreach($etat_lots as $etat_lot)

                            <option value="{{ $etat_lot->id }}"><span class="text-danger">{{ $etat_lot->param1 }}</span></option>
                        
                        @endforeach
                    </select>

            
                </div>        
         
        </div>
            <button type="submit" name="submit" class="btn btn-primary">Valider</button>
    </form>

@endsection

