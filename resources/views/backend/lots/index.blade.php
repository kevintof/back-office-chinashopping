@extends('backend.layouts.app')

    @section('button')

        <a type="button" class="btn btn-success " href="{{route('admin.lots.create')}}">Créer un lot</a>
     
    @endsection

	@section('page')
		Lots
	@endsection



	@section('content')


        <div class="row mt-3">
            <div class="col-lg-12">
                <div class="card">

                    <div class="card-header">

                        <div class="float-left">
                            <p class="lead">
                                <i class="fa fa-globe-africa"></i>
                                <strong> Liste des Lots</strong>
                            </p>
                        </div>
                    </div>


                    <!-- /.card-header -->

                    <div class="card-body">
                        @if ($message = Session::get('success'))

                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                {{ $message }}
                            </div>
                        @endif
                        <div id="user_model_details"></div>

                        <table id="listLots" class="table table-bordered table-striped">
                            <thead>

                                <tr>
                                    <th>No</th>
                                    <th>Reference</th>
                                    <th>Date départ</th>
                                    <th>Date arrivé</th>
                                    <th>Nombre de colis</th>
                                    <th>Action</th>
                                </tr>

                            </thead>

                            <tbody>
                                @php ($i = 0)
                                @foreach($lots as $lot)
                                
                                    <tr >

                                        <th>
                                            {{ ++$i }}
                                        </th>
                                        <th >
                                            {{$lot->reference}}
                                        </th>
                                        <th>
                                            {{$lot->date_depart}}
                                        </th>
                                        <th>
                                            {{$lot->date_arrive}}
                                        </th>

                                        <th>
                                            {{ count($lot->colis) }}
                                        </th>
                                       
                                        <th>
                                        
                                            <div class="btn-group">
                                                <a type="button" class="btn btn-success open-data" href="{{ route('admin.lots.show', $lot->id) }}" title="Détails"><i class="nav-icon fas fa-eye"></i></a>
                                                 <a href="{{ route('admin.lots.view', $lot->id) }}" data-toggle="modal" data-target="#exampleModal" type="button" class="btn btn-primary " onclick="setLotId({{$lot->id}})"><i class="fas fa-plus">
                                                      </i></a> 
                                                  <a type="button" class="btn btn-warning" href="{{ route('admin.lots.update', $lot->id) }}" title="Détails" ><i class="nav-icon fas fa-edit"></i></a>
                                                    <a   type="button"  class="btn btn-danger start_chat" href="{{ route('admin.lots.delete', $lot->id) }}" title="delete"><i class="fas fa-trash"></i></a>
                                                    
                                             </div>
                                           
                                        </th>
                                        @include('backend.lots.modal')
                                    </tr>

                                @endforeach
                            </tbody>

                        </table>
                        
                    </div> 
                                   
                </div>
            </div>
        </div>
 

              
    @endsection

    @section('javascripts')

        <script type="text/javascript">
            $(document).ready(function(){

                $("#listLots").DataTable({
                  "responsive": true,
                  "autoWidth": false,
                });
            })
            function setLotId(id) {

                $('#lot_id').val(id);
            }
        </script>
       
    @endsection


   
  
  
