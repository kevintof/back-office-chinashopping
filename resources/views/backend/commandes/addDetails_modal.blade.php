<div class="modal fade" id="addDetails">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><center>DETAILS LIVRAISON</center></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="addDetailsBody">

        <div id="addDetailsBodyMsg">
        </div>

        <div class="row">
          <div class="col-sm-6">
            <!-- text input -->
            <div class="form-group">
              <label>Position</label>
              <textarea class="form-control" id="position" rows="3" placeholder="Ex: Douane Chine"></textarea>
            </div>
          </div>
          <div class="col-sm-6">
            <!-- textarea -->
            <div class="form-group">
              <label>Détails</label>
              <textarea class="form-control" id="comentaire" rows="3" placeholder=""></textarea>
            </div>
          </div>
        </div>

      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="resetAddModal()">Annuler</button>
        <button type="button" class="btn btn-success" id="addDetails" onclick="storeLivraisonDetails({{$commande->details_commande->colis[0]->id}})">Ajouter</button>
        <button type="button" class="btn btn-success" id="editDetails" onclick="updateLivraisonDetails()">Valider mise à jour</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>