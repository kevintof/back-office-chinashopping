@extends('backend.layouts.app')
    
    @section('linkCss')
    @endsection

	@section('page')
		commande détails
	@endsection

    @section('button')

        <a type="button" class="btn btn-primary" href="#">Retour aux commandes</a>
        <input type="submit" value="Ajouter un datails suivi de colis" class="btn btn-success"  data-toggle="modal" data-target="#addDetails">
        <a href="javascript:void(0);" id="{{$commande->id}}" type="button" data-commandeid ="{{$commande->id}}" data-reference="{{$commande->reference}}" class="btn btn-info start_chat" href="" title="Envoyez une note au client par rapport à cette commande"><i class="fas fa-envelope"></i></a>

        Commande de <a href="#">{{$commande->user->nom}} {{$commande->user->prenoms}} </a> | Pays : {{ $commande->user->pays->nom }} | Téléphone : +{{ $commande->user->pays->indicatif }} {{$commande->user->phone_number}}
        
    @endsection

    @section('content')
    <div id="user_model_details"></div>
        @php($coli = $commande->details_commande->colis[0])

        <div class="row">
            <div class="col-md-6">
                <div class="card {{$commande->statut->param5}}" id="commandeInfoform">
                    <div class="card-header">
                        <h3 class="card-title">Information de la commande </h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                  
                    <div class="card-body" id="commandeInfo">
                        <div class="overlay" id="commandeInfoMsg">
                        </div>

                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <a class="commande" id="{{$commande->id}}" href="#"></a>
                                <b>Référence</b> <a class="float-right">{{$commande->reference}}</a>
                            </li>
                            <li class="list-group-item" id="payment">
                                <b>Paiement</b>
                                    @if($commande->details_commande->transaction != null )
                                        <a class="float-right" href="#" >
                                            @if($commande->details_commande->transaction->reference)
                                              {{$commande->details_commande->transaction->reference}}
                                            @else
                                              {{$commande->details_commande->transaction->mobile_payement_reference}}
                                            @endif
                                             
                                        </a>
                                    @else
                                        @if($commande->statut->param2 == 3)
                                            <a href="#" class="btn btn-success float-right" type="button" data-toggle="modal" data-target="#modal-save-payment">PAIEMENT ESPECE</a>
                                        @else
                                            <a class="float-right">AUCUN PAIEMENT EFFECTUE</a>
                                        @endif 
                                    @endif
                            </li>

                            <li class="list-group-item">
                                <div class="form-group">
                                    <label for="statut_id">Statut</label>
                                    <select class="form-control custom-select" id="statut_id" onchange="updateState({{$commande->id}})">
                                        <option selected disabled>Selectionner l'état de la commande actuel</option>
                                        @foreach($etatCommandes as $etat)
                                            @if($etat->id == $commande->statut_id)
                                                <option value="{{$etat->id}}" selected>{{$etat->param1}}</option>
                                            @else
                                                <option value="{{$etat->id}}">{{$etat->param1}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>

                            </li>
                            <li class="list-group-item">
                                <div class="form-group">
                                    <label for="montant_commande">Montant de commande</label>
                                    <h3 class="float-right" id="montant_commande">{{$commande->montant_commande}}</h3>
                                </div>
                            </li>

                            <li class="list-group-item">
                                <div class="form-group">
                                    <label for="montant_service">Frais du service</label>
                                    <h3 class="float-right"><span id="montant_service">{{$commande->montant_service}}</span> </h3>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="form-group">
                                    <label for="transport">Transport</label>
                                    <h3 class="float-right"><span id="transport">{{$coli->transport}}</span> </h3>
                                </div>
                            </li>
                            
                            <li class="list-group-item">
                                <div class="form-group">
                                    <b>Livraison</b>
                                    
                                    @if($coli->adresse_livraison_id != 1)
                                        <a class="float-right">
                                            <input type="number" id="frais_livraison" class="form-control" value="{{ $coli->frais_livraison}}" onchange="saveColis({{ $coli->id}}, 'frais_livraison')">
                                        </a>

                                        <span id="frais_livraisonMsg" class="float-center"></span>
                                    @else
                                        <h3 class="float-right"><span id="frais_livraison">0</span> </h3>
                                    @endif
                                    
                                </div>
                            </li>
                            

                            <li class="list-group-item  bg-warning">
                                <div class="form-group">
                                    <label for="montant_total">Montant total </label>
                                    <h3 class="float-right">
                                        <span id="montant_total">
                                            {{$commande->montant_livraison + $commande->montant_commande + $commande->montant_service}}  F CFA
                                        </span>
                                    </h3>
                                </div>
                            </li>

                            <li class="list-group-item">
                                <div class="form-group">
                                    <label for="information">Information du client sur la commande</label>
                                    <textarea class="form-control" id="information" rows="4" disabled>{{$commande->information}}</textarea>
                                </div>
                            </li>
                       </ul>
                        
                    </div>
                    <!-- /.card-body -->
                </div>
              <!-- /.card -->

                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Les articles commandés</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="card-body">                     
                        
                        <div class="accordion" id="accordion01">
                            @php($i=0)
                            <ul id="product-list" class="list-group list-group-unbordered mb-3">
                            @foreach($coli->produits as $produit)
                                @php($i++)
                                
                                    <li class="list-group-item">

                                        <div class="item">
                                        <div class="accordion-header {{"produit".$produit->id}}">
                                                <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#accordion1a{{$i}}">
                                                <strong>Article {{ $i }}</strong> : <span class="{{"nom".$produit->id}}">{{ $produit->nom }}</span> | <strong> Qte </strong> : <span  id="quantite{{$i}}" class="{{"quantite".$produit->id}}">{{ $produit->quantite }}</span> 
                                                </button>
                                            </div>

                                            <div id="accordion1a{{$i}}" class="accordion-body collapse" data-parent="#accordion01">
                                                <div id='accordionContent{{$i}}' class="accordion-content">
                                                    <div class="overlay" id="productMsg{{$i}}">
                                                    </div>

                                                    <form>
                                                    
                                                        <div class="card">
                                                          <div class="card-body">
                                                            <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                                                                <p class="text-left col-md-3">
                                                                    <strong>Description</strong>
                                                                </p>
                                                            <p class="d-flex flex-column text-justify {{"description".$produit->id}}">
                                                                    {{ $produit->description }}
                                                                </p>
                                                            </div>
                                                            <!-- /.d-flex -->

                                                            <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                                                                <p class="text-left col-md-3">
                                                                    <strong>Prix unitaire</strong>
                                                                </p>
                                                                <p class="d-flex flex-column text-right col-md-6">
                                                                    @if($produit->montant != null || $produit->montant != '')
                                                                        <input type="number" id="montant{{$i}}" class="form-control" value="{{ $produit->montant }}">
                                                                    @else
                                                                        <input type="number" id="montant{{$i}}" class="form-control"  value="0">
                                                                    @endif
                                                                </p>
                                                                <p class="d-flex flex-column col-md-3">
                                                                    F CFA
                                                                </p>
                                                            </div>
                                                            <!-- /.d-flex -->

                                                            <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                                                                <p class="text-left col-md-3">
                                                                    <strong>Poids ou volume</strong>
                                                                </p>
                                                                <p class="d-flex flex-column text-right col-md-6">
                                                                    <input type="number" id="poids{{$i}}" class="form-control">
                                                                </p>
                                                                <p class="d-flex flex-column col-md-3">
                                                                    kg ou m3
                                                                </p>
                                                            </div>
                                                            <!-- /.d-flex -->

                                                            <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                                                                <p class="text-left col-md-3">
                                                                    <strong>Statut</strong>
                                                                </p>
                                                                <p class="d-flex flex-column text-right col-md-6 ">
                                                                    <select class="form-control custom-select" id="statut{{$i}}">
                                                                        @if($produit->statut != null)
                                                                            <option selected value="{{$produit->statut}}"> {{$produit->statut}} </option>
                                                                        @else
                                                                            <option value="DISPONIBLE"> DISPONIBLE </option>
                                                                        @endif
                                                                            
                                                                        <option disabled></option>
                                                                    
                                                                        <option value="DISPONIBLE"> DISPONIBLE </option>
                                                                        <option value="NON DISPONIBLE"> NON DISPONIBLE </option>
                                                                            
                                                                    </select>
                                                                </p>
                                                                <p class="d-flex flex-column col-md-3">
                                                                   
                                                                </p>
                                                            </div>
                                                            <!-- /.d-flex -->
                                                            <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                                                                <p class="text-left col-md-4">
                                                                <span><a href="{{ $produit->lien }}" class="{{"lien".$produit->id}}"  target="_blank">Voir le produit</a></span>
                                                                </p>
                                                                <p class="d-flex flex-column text-right">
                                                                    <input type="button" value="Enregistrer les modifications" class="btn btn-success" onclick="saveProduct({{$produit->id}},{{$i}}, {{count($coli->produits)}}, {{$commande->id}})">
                                                                </p><br/>
                                                                
                                                            </div>
                                                        <button data-produitid={{$produit->id}} data-quantite="{{$produit->quantite}}" data-lien="{{$produit->lien}}" data-description="{{$produit->description}}" data-nom="{{$produit->nom}}" data-toggle="modal" data-target="#modal-default" type="button" class="btn  btn-outline-primary btn-lg float-right edit-product">Modifier l'article</button>
                                                          </div>
                                                          
                                                        </div>
                                                    </form>
                                                       
                                                </div>
                                            </div>
                                            
                                        </div>
                                       
                                    </li>
                                   
                                
                            @endforeach
                            </ul>
                            <button type="button" data-toggle="modal" data-target="#modal-create" class="btn  bg-gradient-primary btn-lg float-right"><i class="fa fa-plus"></i> &nbsp; Ajouter un produit</button>
                        </div>
                        
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>

            <div class="col-md-6">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title">Détails de la livraison</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                              <i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>Livraison prévu le </b> 
                                <a class="float-right">
                                    @if($commande->details_commande->date_prevu_livraison_max != null )
                                        {{$commande->details_commande->date_prevu_livraison_max}}
                                    @else
                                        NON DISPONIBLE
                                    @endif
                                    
                                </a>
                            </li>

                            <li class="list-group-item">
                                <b>Date de livraison </b> 
                                <a class="float-right">
                                    @if($commande->details_commande->date_livraison != null )
                                        {{$commande->details_commande->date_livraison}}
                                    @else
                                        NON DISPONIBLE
                                    @endif
                                </a>
                            </li>

                            <li class="list-group-item" id="livraison">
                                <b>Mode de livraison </b> 
                                <a class="float-right">
                                    @if($coli->type_livraison != null )
                                        <a class="float-right val_livraison" href="#" >
                                            {{$coli->type_livraison->param1}}
                                        </a>
                                        <br/><br/>
                                        <button  data-toggle="modal" data-target="#modal-edit-livraison" type="button" class="btn  btn-outline-primary btn-lg float-right edit-product">Modifier le mode de livraison</button>
                                    @else
                                        NON DISPONIBLE
                                    @endif
                                </a>
                            </li>

                            <li class="list-group-item">
                                <b>Référence du colis </b> 
                                <a class="float-right" href="#">
                                    {{$coli->reference}}
                                </a>
                            </li>

                            <li class="list-group-item">
                                <b>Adresse de livraison </b> 
                                <div class="callout callout-info">
                                    <h5><i class="fas fa-map-marker">   
                                        @if($coli->adresse_livraison->id == 1)
                                            Adresse de l'agence
                                        @endif
                                    </i> {{ $coli->adresse_livraison->ville}} {{ $coli->adresse_livraison->pays}} </h5>

                                    {{ $coli->adresse_livraison->quartier}}
                                    {{ $coli->adresse_livraison->ville}}
                                    {{ $coli->adresse_livraison->adresse1}}
                                    {{ $coli->adresse_livraison->adresse2}}

                                </div>
                            </li>

                            <li class="list-group-item">
                                <div class="form-group">
                                    <b>Poids(kg) / Volume(m3) du colis </b>
                                    <a class="float-right">
                                        <input type="number" id="poids" class="form-control" value="{{ $coli->poids}}"  step="0.0000001" onchange="saveColis({{ $coli->id}}, 'poids')">
                                    </a>
                                    
                                    <span id="poidsMsg" class="float-center"></span>
                                    
                                </div>
                            </li>

                            <li class="list-group-item">
                                <div class="form-group">
                                    <label for="inputEstimatedDuration">Galerie photo du colis / des produits</label>

                                    <div class="row mb-3" id='preview'>

                                        @if($coli->images == '')
                                            AUCUNE IMAGE DISPONIBLE
                                        @else
                                            @php($images = json_decode($coli->images))
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    
                                                        @if($images->user)
                                                            @foreach( $images->name as $imagesName)
                                                               <div class="col-sm-6"><img class="img-fluid mb-3" src="/commandes-colis/{{$images->user}}/{{$imagesName}}" alt="Photo"></div>
                                                            @endforeach
                                                         @endif
                                                    
                                                </div>
                                            </div>
                                        @endif     

                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="btn btn-info btn-file">
                                        <i class="fas fa-paperclip"></i> Ajouter une ou plusieur images du colis
                                        <input type="file" id='files' name="files[]" multiple><br>
                                    </div>

                                    <p class="help-block">Max. 32MB</p> <input type="button" class="btn btn-success" id="submit_images" onclick="uploadImages({{$coli->id}})" value='Télécharger les images'>
                                </div>
                            </li>
                        </ul>                      
                    </div>
                <!-- /.card-body -->
                </div>
              <!-- /.card -->
                <div class="card card-info">
                    <div class="card-header">
                      <h3 class="card-title">Suivi du colis</h3>

                      <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                          <i class="fas fa-minus"></i></button>
                      </div>
                    </div>
                    <div class="card-body p-0">

                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Position</th>
                                    <th>Détails</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                @if(count($coli->livraison_details) != 0)
                                    @foreach($coli->livraison_details as $details)
                                        <tr>
                                            <td id="position{{$details->id}}">{{$details->position}}</td>
                                            <td id="commentaire{{$details->id}}">{{$details->commentaire}}</td>
                                            <td>{{$details->date}}</td>
                                            <td class="text-right py-0 align-middle">
                                                <div class="btn-group btn-group-sm">
                                                    <a href="#" class="btn btn-info"  data-toggle="modal" data-target="#addDetails" onclick="editLivraisonDetails({{$details->id}})"><i class="fas fa-edit"></i></a>
                                                    <a href="#" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr> 
                                        <td colspan="4" align="center">AUCUN SUIVI ENREGISTRE</td>
                                        
                                    </tr>
                                @endif
                                
                                
                            </tbody>
                        </table>

                        @include('backend.commandes.addDetails_modal')
                        @include('backend.commandes.modal')

                    </div>
                <!-- /.card-body -->
                </div>
              <!-- /.card -->
            </div>


        </div>
        <div class="row">
            <div class="col-12">
                <a type="button" class="btn btn-primary" href="#">Retour aux commandes</a>
                <input type="submit" value="Ajouter un datails suivi de colis" class="btn btn-success"  data-toggle="modal" data-target="#addDetails">
                <a href="javascript:void(0);" id="{{$commande->id}}" type="button" data-commandeid ="{{$commande->id}}" data-reference="{{$commande->reference}}" class="btn btn-info start_chat" href="" title="Envoyez une note au client par rapport à cette commande"><i class="fas fa-envelope"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                </br>
            </div>
        </div>
        <div class="modal fade" id="modal-default">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Modifier l'article</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                     <form>
                            <input type="hidden"  class="form-control modal-prod-id"  >
                            <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                                    <p class="text-left col-md-3">
                                        <strong>Nom</strong>
                                    </p>
                                    <p class="d-flex flex-column text-right col-md-6">
                                            <input type="text"  class="form-control modal-nom" value="0" >
                                    </p>
                                    
                            </div>
                            <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                                    <p class="text-left col-md-3">
                                        <strong>Description</strong>
                                    </p>
                                    <p class="d-flex flex-column text-right col-md-6">
                                            <input type="text"  class="form-control modal-description"  >
                                    </p>
                                    
                            </div>
                            <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                                    <p class="text-left col-md-3">
                                        <strong>Quantité</strong>
                                    </p>
                                    <p class="d-flex flex-column text-right col-md-6">
                                            <input type="number"  class="form-control modal-quantite" >
                                    </p>
                                    
                            </div>
                            <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                                    <p class="text-left col-md-3">
                                        <strong>Lien </strong>
                                    </p>
                                    <p class="d-flex flex-column text-right col-md-6">
                                        <input type="text" placeholder="Ex: alibaba.com/store/sneakers" class="form-control modal-lien" >
                                    </p>
                                    
                            </div>
                     </form>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                  <button type="button" class="btn btn-primary send-data">Enregistrer</button>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->

          @include('backend.commandes.create-product-modal')
          @include('backend.commandes.payment-modal')
          @include('backend.commandes.edit_mode_livraison_modal')
          @endsection

@section('javascripts')
<script src="{{ asset('admin/dist/js/app/commandes.js') }}"></script>
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
     <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
    
    <script src="{{ asset('app-js/commande/chat.js') }}"></script> 
    <script src="{{asset('admin/badge_notifications.js')}}"></script>
    <script src="http://malsup.github.com/jquery.form.js"></script>

    <script src="{{ asset('admin/dist/js/app/commande.show.js') }}"></script>
    <script src="{{asset('app-js/commande/edit-commande.js')}}"></script>
    <script src="{{asset('app-js/commande/add-transaction.js')}}"></script>
    <script src="{{asset('app-js/commande/edit-mode-livraison.js')}}"></script>
    
   
    
    <script>
        $(document).ready(function(){
            var commande_id = $('.commande').attr('id');
            update_commande_seen_statut(commande_id);
    

            function update_commande_seen_statut(id){
                
                $.ajax({
                    url: "{{route('admin.commandes.notification.update',['id'=>$commande->id])}}",
                    method:"GET",
                    success:function(data){
                    } 
                })
            }

        });
    </script>
    <script src="{{asset('admin/badge_notifications.js')}}"></script>
@endsection