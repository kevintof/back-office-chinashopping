<div class="modal fade" id="modal-save-payment">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Enregistrement de paiement</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <form id="add-transaction" method="post"  action="{{route('admin.commande.addtransaction',['id'=>$commande->id])}}">
                
                    @csrf 
                    {{-- <input type="hidden"  class="form-control modal-prod-id"  > --}}
                    <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                            <p class="text-left col-md-3">
                                <strong>Moyen de paiement</strong>
                            </p>
                            <p class="d-flex flex-column text-right col-md-6">
                              <select class="form-control custom-select" id="mode_payment" name="mode_paiement" >
                                <option selected disabled>Selectionner le moyen de paiement</option>
                                @foreach($paymentTypes as $payment)
                                    {{-- @if($etat->id == $commande->statut_id)
                                        <option value="{{$etat->id}}" selected>{{$etat->param1}}</option>
                                    @else --}}
                                        <option value="{{$payment->id}}">{{$payment->param1}}</option>
                                    {{-- @endif --}}
                                @endforeach
                            </select>
                            </p>
                            
                    </div>
                    <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                            <p class="text-left col-md-3">
                                <strong>Référence</strong>
                            </p>
                            <p class="d-flex flex-column text-right col-md-6">
                                    <input type="text" name="reference" id="payment-reference" class="form-control">
                            </p>
                            
                    </div>
                    <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                            <p class="text-left col-md-3">
                                <strong>Montant</strong>
                            </p>
                            <p class="d-flex flex-column text-right col-md-6">
                                <input type="number" id="payment-montant" name="montant" class="form-control" >
                            </p>
                            
                    </div>
                    <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                      <p class="text-left col-md-3">
                          <strong>Numero de telephone </strong>
                      </p>
                      <p class="d-flex flex-column ">
                        <input type="tel" id="phone_number" name="phone_number" class="form-control" >
                      </p>
                      
                   </div>
                    <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                      <p class="text-left col-md-3">
                          <strong>Date de paiement</strong>
                      </p>
                      <p class="d-flex flex-column ">
                        <input type="datetime-local" id="payment-date" name="date" class="form-control" >
                      </p>
                      
                   </div>
                   <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                    <p class="text-left col-md-3">
                        <strong> Commentaires</strong>
                    </p>
                    <p class="d-flex flex-column ">
                      <textarea  id="payment-comment" name="commentaire" class="form-control" ></textarea>
                    </p>
                    
                 </div>
                  {{--   <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                            <p class="text-left col-md-3">
                                <strong>Lien </strong>
                            </p>
                            <p class="d-flex flex-column text-right col-md-6">
                                <input type="text" id="add-produit-lien" name="lien" placeholder="Ex: alibaba.com/store/sneakers" class="form-control " >
                            </p>
                            
                    </div> --}}
             </form>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
          <button id="save-payment" type="button" class="btn btn-primary savePayment ">Enregistrer</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

