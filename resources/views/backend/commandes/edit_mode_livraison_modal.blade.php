<div class="modal fade" id="modal-edit-livraison">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Modification du mode de livraison</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <form id="edit-livraison" method="post"  action="{{route('admin.commande.edit-livraison',['id'=>$commande->id])}}">
                
                    @csrf 
                    {{-- <input type="hidden"  class="form-control modal-prod-id"  > --}}
                    <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                            <p class="text-left col-md-3">
                                <strong>Moyen de Livraison</strong>
                            </p>
                            <p class="d-flex flex-column text-right col-md-6">
                              <select class="form-control custom-select" id="mode_payment" name="mode_livraison" >
                                <option selected disabled>Selectionner le moyen de livraison</option>
                                @foreach($livraisonTypes as $livraison)
                                    {{-- @if($etat->id == $commande->statut_id)
                                        <option value="{{$etat->id}}" selected>{{$etat->param1}}</option>
                                    @else --}}
                                        <option value="{{$livraison->id}}">{{$livraison->param1}}</option>
                                    {{-- @endif --}}
                                @endforeach
                            </select>
                            </p>
                            
                    </div>
                    
                  
             </form>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
          <button id="mode-livraison" type="button" class="btn btn-primary saveModeLivraison">Enregistrer</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

