@extends('backend.layouts.app')

    @section('button')
        <a type="button" class="btn btn-success" href="">Ajouter une nouvelle commande </a>             
    @endsection

	@section('page')
		Commandes
	@endsection

	@section('content')
                 
        {{-- <div class="row p-2 clearfix">

            <div class="form-group clearfix">
                <div class="icheck-success d-inline">
                    <span> <b> Ignorer période </b> </span><br>
                    <input type="checkbox" id="ignore_periode" name="ignore_periode">
                    <label for="ignore_periode">
                    </label>
                </div>
            </div>
            


            
            <div class="col-12 col-sm-4 col-md-3">
                <span> <b> Du </b> </span>
                <input type="date" name="date_debut" class="form-control" id="date debut">
            </div>

            
            <div class="col-12 col-sm-4 col-md-3">
                <span> <b> Au </b> </span>
                <input type="date" name="date_fin" class="form-control" id="date fin">
            </div>

            
            <div class="col-6 col-sm-3 col-md-3">
                <span> <b> Etat </b> </span>
                <select class="form-control" name="statut_id"> 
                    <option value="0" selected>Tout</option> 
                    <option value="0"></option> 

                    @foreach($etatCommandes as $etat)
                        <option value="{{$etat->id}}">{{$etat->param1}}</option>
                    @endforeach
                </select>
            </div>

            <div class="mt-4">
                <span class="info-box-icon bg-info elevation-1">
                    <a type="button" class="btn btn-success filtreCommande" href="">
                        <i class="fas fa-search"> </i></a>
                </span>
            </div>
            
        </div> --}}
        <div id="accordion">
            <div class="card card-info">
                <div class="card-header">
                    <h4 class="card-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#filterCommandes" class="collapsed" aria-expanded="false">
                            Filtrer les commandes
                        </a>
                    </h4>
                </div>
                <div id="filterCommandes" class="panel-collapse collapse" style="">
                    <div class="card-body">
                        <div class="row p-2 clearfix">
                                
                                <div class="col-sm-2">
                                    <!-- checkbox -->
                                    <div class="form-group clearfix">
                                        <label> Ignorer période </label> <br>
                                        <div class="icheck-success d-inline">
                                            <input type="checkbox" id="ignore_periode" name="ignore_periode" checked="">
                                            <label for="ignore_periode"></label>
                                        </div>

                                    </div>
                                </div>
                                
                                <div class="col-12 col-sm-4 col-md-3">
                                    <span> <b> Du </b> </span>
                                    <input type="date" class="form-control" id="date debut" name="date_debut">
                                </div>

                                <div class="col-12 col-sm-4 col-md-3">
                                    <span> <b> Au </b> </span>
                                    <input type="date" class="form-control" id="date fin" name="date_fin">
                                </div>
                                
                                <div class="col-6 col-sm-3 col-md-3">
                                    <span> <b> Etat </b> </span>
                                    <select class="form-control" id="statut_id" name="statut_id"> 
                                        <option value="0" selected="">Tout</option> 
                                        <option value="0"></option> 

                                                                                    <option value="1">En cours de Cotation</option>
                                                                                    <option value="2">En cours d'exécussion</option>
                                                                                    <option value="3">En attente de paiement</option>
                                                                                    <option value="4">Suspendu</option>
                                                                                    <option value="5">Refusé</option>
                                                                                    <option value="6">Terminé</option>
                                                                                    <option value="25">Annulé</option>
                                                                                    <option value="27">En attente de livraison</option>
                                                                            </select>
                                </div>

                                <div class="mt-4">
                                    <span class="info-box-icon bg-info elevation-1">
                                        <a type="button" class="btn btn-success filtreCommande" href="">
                                            <i class="fas fa-search"> </i>
                                        </a>
                                    </span>
                                </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-3">
            
            <div class="col-lg-12">
                <div class="card">

                    <div class="card-header">

                        <div class="float-left">
                            <p class="lead">
                                <i class="fab fa-shopify"></i>
                                <strong> Liste des commandes</strong>
                            </p>
                        </div>

                        <div class="float-right">
                            <p class="lead">
                                <strong> Commandes enregistrées : <b>{{ count($commandes) }}</b> </strong>
                            </p>
                        </div>
                    </div>

                    <div class="card-body">
                        <div id="user_model_details"></div>
                        <table id="listCommande" class="table table-bordered table-striped">

                            <thead>

                                <tr>
                                    <th>No</th>
                                    <th>Reference</th>
                                    <th>Utilisateur</th>
                                    <th>Etat</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                @php ($i = 0)
                                @foreach($commandes as $commande)
                                    <tr @if($commande->lu_par_administration() == false ) style="background-color: lightsalmon" @endif class="commande" data-commandeid="{{$commande->id}}">

                                        <th>
                                            {{ ++$i }}
                                        </th>
                                        <th class="reference">
                                            {{ $commande->reference }}
                                        </th>
                                        <th>
                                            <a href="{{ route('admin.user.show', $commande->user->id) }}"> {{ $commande->user->nom }} {{ $commande->user->prenoms }} | {{ $commande->user->phone_number }} </a>
                                        </th>
                                        <th>
                                            {{ $commande->statut->param1 }}
                                        </th>
                                        <th>
                                            {{ $commande->details_commande->date_commande }}
                                        </th>
                                        <th>
                                            <div class="btn-group">
                                                <a type="button" class="btn btn-success" href="{{ route('admin.commande.show', $commande->id) }}" title="Détails"><i class="nav-icon fas fa-eye"></i></a>
                                            <a href="javascript:void(0);" id="{{$commande->id}}" type="button" data-commandeid ="{{$commande->id}}" data-reference="{{$commande->reference}}" class="btn btn-info start_chat" href="" title="Envoyez une note au client par rapport à cette commande"><i class="fas fa-envelope"></i> @if($commande->messages_non_lus() !=0 ) <span class="badge badge-warning navbar-badge " style="">{{$commande->messages_non_lus()}}</span>@endif </a>
                                                
                                            </div>
                                        </th>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
  
    @endsection

@section('javascripts')
    <script src="{{ asset('admin/dist/js/app/commandes.js') }}"></script>
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
    
    <script src="{{ asset('app-js/commande/chat.js') }}"></script> 
    <script src="{{asset('admin/badge_notifications.js')}}"></script>
    <script src="https://malsup.github.com/jquery.form.js"></script>
    <script>
        $(document).ready(function(){

            $("#listCommande").DataTable({
              "responsive": true,
              "autoWidth": false,
            });


            var cmd_ids = $('tbody tr');
            var i =0;

            
           
        })
    </script>    
@endsection