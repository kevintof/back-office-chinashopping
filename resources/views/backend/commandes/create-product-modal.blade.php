<div class="modal fade" id="modal-create">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Ajouter l'article</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <form id="create-product" method="post" action="{{route('admin.commande.addproduct',['id'=>$commande->id])}}">
                
                    @csrf 
                    {{-- <input type="hidden"  class="form-control modal-prod-id"  > --}}
                    <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                            <p class="text-left col-md-3">
                                <strong>Nom</strong>
                            </p>
                            <p class="d-flex flex-column text-right col-md-6">
                                    <input id="add-produit-nom" type="text"  class="form-control" name="nom" >
                            </p>
                            
                    </div>
                    <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                            <p class="text-left col-md-3">
                                <strong>Description</strong>
                            </p>
                            <p class="d-flex flex-column text-right col-md-6">
                                    <input type="text" name="description" id="add-produit-description" class="form-control "  >
                            </p>
                            
                    </div>
                    <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                            <p class="text-left col-md-3">
                                <strong>Quantité</strong>
                            </p>
                            <p class="d-flex flex-column text-right col-md-6">
                                    <input type="number" id="add-produit-quantite" name="quantite" class="form-control" >
                            </p>
                            
                    </div>
                    <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                            <p class="text-left col-md-3">
                                <strong>Lien </strong>
                            </p>
                            <p class="d-flex flex-column text-right col-md-6">
                                <input type="text" id="add-produit-lien" name="lien" placeholder="Ex: alibaba.com/store/sneakers" class="form-control " >
                            </p>
                            
                    </div>
             </form>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
          <button id="add-product-to-commande" type="button" class="btn btn-primary">Enregistrer</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

