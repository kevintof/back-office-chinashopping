<div class="modal fade" id="addDetails">
  <div class="modal-dialog">

    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><center>DETAILS LIVRAISON COLIS</center></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body" id="addDetailsBody">

        <div id="addDetailsBodyMsg"></div>
        <form action="{{ route('admin.colis.details') }}" method="post">
          @csrf
          <div class="row">
            <div class="col-sm-6">
              <!-- text input -->
              <div class="form-group">
                <label>Position</label>
                <textarea class="form-control" id="position" rows="3" placeholder="Ex: Douane Chine" name="position"></textarea>
              </div>
            </div>

            <div class="col-sm-6">
              <!-- textarea -->
              <div class="form-group">
                <label>Commentaire</label>
                <textarea class="form-control" id="comentaire" rows="3" placeholder="" name="commentaire"></textarea>
              </div>
            </div>

              <input type="hidden" id="colis_id"  name="colis_id">
              <input type="hidden" id="all"  name="all">
            <button  class="btn btn-success" type="submit"> Valider </button>
          </div>
        </form>
      </div>

      <div class="modal-footer justify-content-between"> </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>