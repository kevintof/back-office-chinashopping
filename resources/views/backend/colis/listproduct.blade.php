@extends('backend.layouts.app')

    @section('button')

   
     
    @endsection

	@section('page')
		Produits
	@endsection



	@section('content')


        <div class="row mt-3">
            <div class="col-lg-12">
                <div class="card">

                    <div class="card-header">

                        <div class="float-left">
                            <p class="lead">
                                <i class="fa fa-cube"></i>
                                <strong> Liste des produits du colis - Ref : {{ $produits[0]->colis->reference }}</strong>
                            </p>
                        </div>
                    </div>

                    <!-- /.card-header -->

                    <div class="card-body">
                    
                        <div id="user_model_details"></div>
                        <table id="lisProduits" class="table table-bordered table-striped">
                            <thead>

                                <tr>
                                    <th> No </th>
                                    <th> Nom </th>
                                    <th> Description </th>
                                    <th> Poids </th>
                                    <th> Quantité </th>
                                    <th> Images </th>
                                </tr>
                            </thead>

                            <tbody>
                                @php ($i = 0)
                                @foreach($produits as $produit)
                                
                                    <tr >

                                        <th>
                                            {{ ++$i }}
                                        </th>

                                        <th >
                                            {{ $produit->nom }}
                                        </th>

                                        <th>
                                            {{ $produit->description }}
                                        </th>

                                        <th>
                                            {{ $produit->poids }}
                                        </th>
                                      
                                        <th>
                                            {{ $produit->quantite }}
                                        </th>

                                        <th>
                                            @if($produit->images != '')
                                                <img class="img-fluid mb-3" src="/commandes-colis/{{$produit->colis->details_commande->commande->user_id}}/{{ $produit->images }}" alt="Photo">
                                            @endif
                                        </th>    
                                    </tr>

                                @endforeach
                           </tbody>

                        </table>
                    </div>                                   
                </div>
            </div>
        </div>
    @endsection

    @section('javascripts')

        <script type="text/javascript">

            $(document).ready(function(){
                $("#lisProduits").DataTable({
                  "responsive": true,
                  "autoWidth": false,
                });
            })
        </script>
       
    @endsection