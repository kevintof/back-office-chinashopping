@extends('backend.layouts.app')

  

	@section('page')
		Colis
	@endsection



	@section('content')

    <div class="row mt-3">
        <div class="col-lg-12">
            <div class="card">

                <div class="card-header">

                    <div class="float-left">
                        <p class="lead">
                            <i class="fa fa-cube"></i>
                            <strong> Colis du Lot -- Ref : {{ $lot_reference }}</strong>
                        </p>
                    </div>
                    <div class="float-right">
                        <a type="button" data-toggle="modal" data-target="#showlivraisonDetails" href="" class="btn btn-info" onclick="getDetailsLivraison({{$colis[0]->lots->id}})"><i class="fas fa-eye"></i> Afficher détails de livraison </a>

                        @if(count($colis) > 0)
                            <a id="" type="button" data-toggle="modal" data-target="#addDetails" class="btn btn-info" href="" onclick="setLotId({{$colis[0]->lots->id}}, 1)"><i class="fas fa-plus"></i> Appliquez Détails livraison à tous les colis</a>
                        @else
                            Auncun colis enregisté
                        @endif
                        
                    </div>
                </div>


                <!-- /.card-header -->

                <div class="card-body">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                            </button>
                            {{ $message }}
                        </div>
                    @endif
                    <div id="user_model_details"></div>
                    <table id="listColis" class="table table-bordered table-striped">
                        <thead>

                        <tr>
                            <th>No</th>
                            <th>Reference</th>
                            <th>Poids</th>
                            <th>Nombre produit</th>
                            <th>Action</th>
                        </tr>

                        </thead>

                        <tbody>
                            @php ($i = 0)
                            @foreach($colis as $coli)
                                <tr>

                                    <th>
                                        {{ ++$i }}
                                    </th>
                                    <th >
                                        {{ $coli->reference }}
                                    </th>
                                    <th>
                                        {{ $coli->poids }}
                                    </th>
                                    <th>
                                        {{ count($coli->produits) }}
                                    </th>
                                    <th>
                                        <div class="btn-group">
                                            <a type="button" class="btn btn-success" href="{{route('admin.product.list',$coli->id)}}" title="Détails"><i class="nav-icon fas fa-eye"></i></a>

                                            <a  id="" type="button" data-toggle="modal" data-target="#addDetails" class="btn btn-info start_chat" href="" onclick="setLotId({{$coli->id}}, 0)"><i class="fas fa-plus"></i></a>
                                            
                                        </div>
                                    </th>
                                </tr>
                            @endforeach
                            @include('backend.colis.addDetails_modal')
                            @include('backend.colis.showDetails_modal')
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

  
    @endsection
   

    @section('javascripts')

        <script type="text/javascript">

            $(document).ready(function(){
                $("#listColis").DataTable({
                  "responsive": true,
                  "autoWidth": false,
                });
            })

            function setLotId(id, all) {

                $('#colis_id').val(id);
                $('#all').val(all);
            }

            function getDetailsLivraison(coli_id) {
                
                $.get({
                    url: "/admin/livraison_details/getAll/"+coli_id ,
                    success: function(data){
                        $('#details_livraison').html(data.details);                     
                    },
                    error: function(msg) {

                    }
                });
            }
        </script>
       
    @endsection
