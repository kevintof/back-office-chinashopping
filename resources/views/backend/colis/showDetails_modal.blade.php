<div class="modal fade" id="showlivraisonDetails">

  <div class="modal-dialog">
    <div class="modal-content">
      

      <div class="modal-header">
        <h4 class="modal-title">DETAILS DE LIVRAISON</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="details_livraison">
        

      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Quitter</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>