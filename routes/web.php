<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Admin routes
Route::prefix('admin')->group(function(){

	// Auth routes
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login');
    Route::post('/logout','Auth\LoginController@logout')->name('admin.logout');

    //Forgot Password Routes
    Route::get('/password/reset','ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('/password/email','ForgotPasswordController@sendResetLinkEmail')->name('password.email');

    //Reset Password Routes
    Route::get('/password/reset/{token}','ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('/password/reset','ResetPasswordController@reset')->name('password.update');


    //Home routes
    Route::get('/home', 'User\Admin\HomeController@index')->name('admin.dashboard');
   
    //Users routes
    Route::get('/users', 'User\Admin\UserController@index')->name('admin.users.index');
    Route::get('/users/show/{id}', 'User\Admin\UserController@show')->name('admin.user.show');
    Route::get('/user/{u_id}/affilies', 'User\Admin\UserController@show_user_affilies')->name('admin.user.show.affilies');


    //Paramètres routes
    Route::get('/parametres', 'User\Admin\ParametresController@index')->name('admin.parametres');

    //All Commandes routes
    Route::get('/commandes', 'User\Admin\CommandesController@index')->name('admin.commandes');
    Route::get('/commandes/getCommandes-{slug}', 'User\Admin\CommandesController@all');
    Route::post('/commandes/search', 'User\Admin\CommandesController@getCommandesByState')->name('admin.commande.filtres');
    //Single commande routes
    Route::get('/commande/show/{id}', 'User\Admin\CommandesController@show')->name('admin.commande.show');
    Route::post('/commande/update-{arg}/{id_commande}', 'User\Admin\CommandesController@update')->name('admin.commande.update');
    Route::get('/commande/check_seen/{id}', 'User\Admin\CommandesController@checkif_commande_is_read_or_not')->name('admin.commande.checkseen');
    Route::post('/commande/update_provided_informations/products/{id}','User\Admin\ProduitsController@update_user_provided_informations')->name('admin.commande.produit.change_primary_informations');       
    Route::post('/commande/add_product/{id}','User\Admin\ProduitsController@addToCommande')->name('admin.commande.addproduct');
    Route::post('/commande/add_transaction/{id}','User\Admin\CommandesController@saveTransaction')->name('admin.commande.addtransaction');
    Route::post('/commande/edit_livraison/{id}','User\Admin\CommandesController@modifierModeLivraison')->name('admin.commande.edit-livraison');
    
    //Colis routes
    Route::get('/colis', 'User\Admin\ColisController@update')->name('admin.ges-colis');
    Route::post('/colis/update-{arg}/{id_colis}', 'User\Admin\ColisController@update')->name('admin.colis.update');

    //Lots routes
    Route::get('/list_lots','User\Admin\LotsController@index')->name('admin.lots.index');
    Route::get('/lot/create','User\Admin\LotsController@create')->name('admin.lots.create');
    Route::post('/lot/send','User\Admin\LotsController@store')->name('admin.lots.store');
    Route::get('/lot/allcolis/{id}','User\Admin\LotsController@show')->name('admin.lots.show');
    //Route::get('/lot/add/{id}','User\Admin\LotsController@add')->name('admin.lots.add');
    Route::post('/addcolis','User\Admin\LotsController@send')->name('admin.lots.send');

    Route::get('/addcolisdetail/{id}','User\Admin\LotsController@view')->name('admin.lots.view');
   
    Route::get('/lots/delete/{id}','User\Admin\LotsController@delete')->name('admin.lots.delete');
    Route::get('/lots/update/{id}','User\Admin\LotsController@update')->name('admin.lots.update');
    Route::post('/lots/insert/','User\Admin\LotsController@insert')->name('admin.lots.insert');

    // adding Details colis route    
    Route::post('/details', 'User\Admin\ColisController@createDetails')->name('admin.colis.details');
    Route::get('/produits/{id}', 'User\Admin\ColisController@listProduct')->name('admin.product.list');


    //Produit routes
    Route::post('/product/update/{id}', 'User\Admin\ProduitsController@update')->name('admin.product.update');

    //Livraison details routes
    Route::post('/livraison_details/store', 'User\Admin\LivraisonDetailsController@store')->name('admin.livraison_details.store');
    Route::post('/livraison_details/update/{id_details}', 'User\Admin\LivraisonDetailsController@update')->name('admin.livraison_details.update');
    Route::get('/livraison_details/getAll/{coli_id}', 'User\Admin\LivraisonDetailsController@getLivraisonDerailsByColi')->name('admin.livraison_details.getLivraisonDerailsByColi');

    //Messages



    //Messagerie de service clientele
    Route::get('/messages', 'User\Admin\MessageController@index')->name('admin.ges-messages');
    //end Messagerie de service clientele
    
    Route::get('/messages/{id}','User\App\MessageController@getMessagesfromCommande')->name('admin.commande.messages');
    Route::get('/commandes/{id}/messages','MessageController@fetch_client_messages');
    Route::get('/messages/clientele/{id}','MessageClientController@fetch_client_messages')->name('admin.clients.messages');



    //Notifications
    Route::get('/notifications/commandes','User\Admin\NotificationController@getCommandesNotifications')->name('admin.commandes.notifications');
    Route::get('/notifications/commandes/update/{id}','User\Admin\NotificationController@updateNotificationState')->name('admin.commandes.notification.update');


    //All lots
    Route::get('/lots','User\Admin\LotsController@index')->name('admin.lots.index');
    Route::get('/lots/show/{id}','User\Admin\LotsController@show')->name('admin.lots.show');
    Route::get('/lots/create', 'User\Admin\LotsController@create')->name('admin.lots.create');
    Route::post('/lots/store','User\Admin\LotsController@store')->name('admin.lots.store');
    Route::get('/lots/etit/{id}','User\Admin\LotsController@edit')->name('admin.lots.edit');
    Route::put('/lots/update/{id}','User\Admin\LotsController@update')->name('admin.lots.update');
    Route::get('/notifications/commandes/{id}/messages-non-lus','User\Admin\CommandesController@count_commande_messages_not_read_byAdmin')->name('admin.commandes.message-non-lus');
    Route::get('/notifications/commandes/{id}/update-messages-status','User\Admin\CommandesController@update_commande_messages_readByadmin')->name('admin.commandes.update-messages.state');
    Route::get('/notifications/header/messages','User\Admin\NotificationController@headMessagesNotification')->name('admin.commandes.head-notifications');
    Route::get('/notifications/header/commandes','User\Admin\NotificationController@get_head_commande_notification')->name('admin.commandes.head-notifications-commande');
    Route::get('/notifications/sidebar/services-clientele/messages-non-lus','User\Admin\NotificationController@service_clientele_all_not_read_messages')->name('admin.messages.sidebar-notifications');
});



Route::get('/login', 'Auth\LoginController@showLoginForm')->name('user.login');
Route::post('/login', 'Auth\LoginController@login')->name('user.login.submit');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/register', 'Auth\RegisterController@showRegisterForm')->name('user.register');
Route::post('/register', 'Auth\RegisterController@create')->name('user.register.submit');
Route::post('/messages/send/{id}','MessageController@sendMessage')->name('user.message.send');
Route::post('/messages/send/picture/{id}','MessageController@send_pictures_as_message')->name('user.message.picture.send');
Route::post('propos/service_client/send/{id}','MessageClientController@sendMessage')->name('user.serviceClient.send');

//Ajout articles à ajouter aux mises à jour de Mazama et David

Route::post('articles/save','User\Admin\NewsController@store');

Route::group(['prefix' => 'admin', 'middleware' => ['auth.admins']], function () {
    
    // Route::get('/', 'User\Admin\AdminController@index')->name('admin.dashboard');
});



